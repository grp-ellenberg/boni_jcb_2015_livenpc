import rimdata
import pandas
import numpy
import matplotlib.pylab

def make_scatter_plot(data1, data2, axis, xaxis, yaxis, xlabel, ylabel, event_name):
    
    axis.scatter(data1,data2, s=80, facecolors='none', edgecolors='b')
    for i in range(len(data1)):
        
        index = event_name.iloc[i].split('t')
        event_number = index[1]
        axis.annotate(event_number, xy=(data1.iloc[i], data2.iloc[i]), xytext=(-2.8,-2.8), textcoords = 'offset points' , fontsize = 6.5)
        
        

    axis.set_xlim(xaxis)
    axis.set_ylim(yaxis)
    axis.set_xlabel(xlabel)
    axis.set_ylabel(ylabel)
    
    
    
def make_scatter_plot_CP(data1, data2, axis, xaxis, yaxis, xlabel, ylabel, event_name):
    
    axis.scatter(data1,data2, s=15, facecolors='none', edgecolors='b')
    axis.set_xlim(xaxis)
    axis.set_ylim(yaxis)
    axis.set_xlabel(xlabel)
    axis.set_ylabel(ylabel)