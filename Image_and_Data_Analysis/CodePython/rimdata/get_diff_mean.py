import rimdata
import pandas
import numpy
import matplotlib.pylab
import os
import scipy.stats

## Function to genereate diffence of the mean between Control - treatment
#################################################################


def get_diff_mean(labtek_dataset):
    
    
        data_TE = list(labtek_dataset.TE_diff_norm)  
        label_TE = list(labtek_dataset.treatment)
        
        data_ratio = list((-1)*(labtek_dataset.NE_ER_diff))
        label_ratio = list(labtek_dataset.treatment)
        
        data_er_start = list(labtek_dataset.ER_start_diff)
        label_er_start = list(labtek_dataset.treatment)
        
        data_er_end= list((-1)*(labtek_dataset.ER_end_diff))
        label_er_end = list(labtek_dataset.treatment)
        
        sig_TE = list()
        for i in labtek_dataset.TE_ttest:
            if i <= 0.01:
                sig_TE.append("*")
            else:
                sig_TE.append(" ")
        sig_ratio = list()
        for i in labtek_dataset.NE_ER_ttest:
            if i <= 0.01:
                sig_ratio.append("*")
            else:
                sig_ratio.append(" ")
                
        sig_er_start = list()
        for i in labtek_dataset.ER_start_ttest:
            if i <= 0.01:
                sig_er_start.append("*")
            else:
                sig_er_start.append(" ")
             
        sig_er_end = list()
        for i in labtek_dataset.ER_end_ttest:
            if i <= 0.01:
                sig_er_end.append("*")
            else:
                sig_er_end.append(" ")         
        
      
## Resort the list in desceding order and resort accordingly the label list for treatment    
                
        data_TE_sort = sorted(data_TE, reverse=True )
        data_TE_enumerate= sorted(enumerate(data_TE), key=lambda tup: tup[1], reverse=True)
        
        node_order = [e[0] for e in data_TE_enumerate] 
        
        label_TE_ordered = list()
        for i in range(0, len(label_TE)):
            label_TE_ordered.append(label_TE[node_order[i]])
            
        sign_TE_ordered = list()
        for i in range(0, len(sig_TE)):
            sign_TE_ordered.append(sig_TE[node_order[i]])
 

## Resort the list in desceding order and resort accordingly the label list for treatment     
       
        data_ratio_sort = sorted(data_ratio, reverse=True )
        data_ratio_enumerate= sorted(enumerate(data_ratio), key=lambda tup: tup[1], reverse=True)
        
        node_order = [e[0] for e in data_ratio_enumerate] 
        
        label_ratio_ordered = list()
        for i in range(0, len(label_ratio)):
            label_ratio_ordered.append(label_ratio[node_order[i]] )
        
        sig_ratio_ordered = list()
        for i in range(0, len(sig_ratio)):
            sig_ratio_ordered.append(sig_ratio[node_order[i]])
            
            
 ## Resort the list in desceding order and resort accordingly the label list for treatment     
       
        data_er_start_sort = sorted(data_er_start, reverse=True )
        data_er_start_enumerate= sorted(enumerate(data_er_start), key=lambda tup: tup[1], reverse=True)
        
        node_order = [e[0] for e in data_er_start_enumerate] 
        
        label_er_start_ordered = list()
        for i in range(0, len(label_er_start)):
            label_er_start_ordered.append(label_er_start[node_order[i]] )
        
        sig_er_start_ordered = list()
        for i in range(0, len(sig_er_start)):
            sig_er_start_ordered.append(sig_er_start[node_order[i]])  
            
            
  ## Resort the list in desceding order and resort accordingly the label list for treatment     
       
        data_er_end_sort = sorted(data_er_end, reverse=True )
        data_er_end_enumerate= sorted(enumerate(data_er_end), key=lambda tup: tup[1], reverse=True)
        
        node_order = [e[0] for e in data_er_end_enumerate] 
        
        label_er_end_ordered = list()
        for i in range(0, len(label_er_end)):
            label_er_end_ordered.append(label_er_end[node_order[i]] )
        
        sig_er_end_ordered = list()
        for i in range(0, len(sig_er_end)):
            sig_er_end_ordered.append(sig_er_end[node_order[i]])  
                       
  
        
        
        return (data_TE_sort, label_TE_ordered, sign_TE_ordered, data_ratio_sort, label_ratio_ordered, sig_ratio_ordered, data_er_start_sort, label_er_start_ordered, sig_er_start_ordered, data_er_end_sort, label_er_end_ordered, sig_er_end_ordered )
               
        
        