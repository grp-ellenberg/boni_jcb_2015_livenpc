import rimdata
import pandas
import numpy
import matplotlib.pylab
import scipy.stats


# Function to generate plots for EACH WELL of the labtek 

def plot_well(well_data, labtec, well, output_pdf_1, output_pdf_2, event_passed_tot, treatment, mean_tran_eff):
    print('Proccesing:', labtec, well)
    
    
    # Generate Figure and place text info

    fig = matplotlib.pylab.figure(1,figsize=(8, 11))
    fig.suptitle(treatment + " " + well, fontsize=10, fontweight='bold')
    fig.text(0.52, 0.82, "Mean Translocation Efficiency: " + str(mean_tran_eff),fontsize=12)
    fig.text(0.38, 0.95, "Events Passed: " + event_passed_tot,fontsize=10)

# make plot of translocation efficiency over time with time points as boxplot

    ax1 = fig.add_subplot(3, 2, 1)
    xaxis = ([0,18])
    yaxis = ([0.4,2.5])
    xlabel = "Time Point"
    ylabel = "Norm_RIM_Intensity"
    ax1.axhline(1,linewidth=1, color='g', ls = '--')
    ax1.axhline(mean_tran_eff, linewidth=1, color='g', ls = '--')
    rim_intensity_max_norm = well_data.rim_intensity_max_norm
    rimdata.plot_time_series(rim_intensity_max_norm, ax1, xaxis, yaxis, xlabel, ylabel)

# make plot of ER decay over time with time points as boxplot

    ax2 = fig.add_subplot(3, 2, 3)
    xaxis = ([0,18])
    yaxis = ([0.2,1.2])
    xlabel = "Time Point"
    ylabel = "Norm_ER_Intensity"
    ER_int_norm = well_data.ER_int_norm
    rimdata.plot_time_series(ER_int_norm, ax2, xaxis, yaxis, xlabel, ylabel)    
    
# make plot of ratio NE/ER over time with time points as boxplot

    ax3 = fig.add_subplot(3, 2, 5)
    xaxis = ([0,18])
    yaxis = ([0.8,4])
    xlabel = "Time Point"
    ylabel = "NE/ER Ratio"
    NE_ER_ratio_norm = well_data.NE_ER_ratio_norm
    rimdata.plot_time_series(NE_ER_ratio_norm, ax3, xaxis, yaxis, xlabel, ylabel)       
 
   
    ax4 = fig.add_subplot(3, 2, 6)
    xaxis = ([0,18])
    yaxis = ([0.8,1.4])
    xlabel = "Time Point"
    ylabel = "Norm_Nuc_size"
    Nucl_size_norm = well_data.Nucl_size_norm
    rimdata.plot_time_series(Nucl_size_norm, ax4, xaxis, yaxis, xlabel, ylabel)        
    
    
    
    fig.subplots_adjust(wspace=0.3, hspace=0.3)

    fig.savefig(output_pdf_1, dpi=400)
    fig.clear()
    
    
    fig = matplotlib.pylab.figure(1,figsize=(8, 11))
    fig.suptitle(treatment + " " + well, fontsize=10, fontweight='bold')
    fig.text(0.38, 0.95, "Events Passed: " + event_passed_tot,fontsize=10)
        
 # make scatter plots 
    event_name = well_data.event_name
    ER_int_start = well_data.ER_inten_start
    rim_int_start = well_data.rim_intensity_start
    NE_ER_ratio = well_data.NE_ER_ratio
    tran_efficiency_max = well_data.tran_efficiency_max
    ER_norm_end = well_data.ER_norm_end
    Nucl_size_norm_end = well_data.Nucl_size_norm_end
    Nucle_size_start = well_data.nucle_size_start    
    
 # compute corrrelation between parameter in scatter plot
        
    ER_int_start_corr = scipy.stats.pearsonr(ER_int_start,tran_efficiency_max)
    rim_int_start_corr = scipy.stats.pearsonr(rim_int_start,NE_ER_ratio)       
    NE_ER_ratio_corr = scipy.stats.pearsonr(NE_ER_ratio,tran_efficiency_max)
    ER_norm_end_corr = scipy.stats.pearsonr(ER_norm_end,tran_efficiency_max)
    Nucl_size_norm_end_corr = scipy.stats.pearsonr(Nucl_size_norm_end,tran_efficiency_max)
    Nucle_size_start_corr = scipy.stats.pearsonr(Nucle_size_start,tran_efficiency_max)
    
    ax1 = fig.add_subplot(3, 2, 1)
    xaxis = ([0,50])
    yaxis = ([0.8,3])
    xlabel = "ER_int_start"
    ylabel = "Translocation Efficiency"
    rimdata.make_scatter_plot(ER_int_start,tran_efficiency_max, ax1, xaxis, yaxis, xlabel, ylabel, event_name)  
    ax1.text(0.9, 2.8, 'Corr=' + str(round(ER_int_start_corr[0],3)) + '  p value=' + str(round(ER_int_start_corr[1],3)) , fontsize=6)    
    
    ax2 = fig.add_subplot(3, 2, 2)
    xaxis = ([0,50])
    yaxis = ([0.5,2])
    xlabel = "RIM_int_start"
    ylabel = "NE_ER_ratio_start"
    rimdata.make_scatter_plot(rim_int_start,NE_ER_ratio, ax2, xaxis, yaxis, xlabel, ylabel, event_name)
    ax2.text(0.9, 1.9, 'Corr=' + str(round(rim_int_start_corr[0],3)) + '  p value=' + str(round(rim_int_start_corr[1],3)) , fontsize=6)    

    
    ax3 = fig.add_subplot(3, 2, 3)
    xaxis = ([0.4,2])
    yaxis = ([0.8,3])
    xlabel = "NE_ER_ratio_start"
    ylabel = "Translocation Efficiency"
    rimdata.make_scatter_plot(NE_ER_ratio,tran_efficiency_max, ax3, xaxis, yaxis, xlabel, ylabel, event_name)
    ax3.text(0.45, 2.8, 'Corr=' + str(round(NE_ER_ratio_corr[0],3)) + '  p value=' + str(round(NE_ER_ratio_corr[1],3)) , fontsize=6)

    ax4 = fig.add_subplot(3, 2, 4)
    xaxis = ([0,1.2])
    yaxis = ([0.8,3])
    xlabel = "ER_norm_end"
    ylabel = "Translocation Efficiency"
    rimdata.make_scatter_plot(ER_norm_end,tran_efficiency_max, ax4, xaxis, yaxis, xlabel, ylabel, event_name)
    ax4.text(0.1, 2.8, 'Corr=' + str(round(ER_norm_end_corr[0],3)) + '  p value=' + str(round(ER_norm_end_corr[1],3)) , fontsize=6)  

    ax5 = fig.add_subplot(3, 2, 5)
    xaxis = ([0.8,1.5])
    yaxis = ([0.8,3])
    xlabel = "Nucl_size_norm_end"
    ylabel = "Translocation Efficiency"
    rimdata.make_scatter_plot(Nucl_size_norm_end,tran_efficiency_max, ax5, xaxis, yaxis, xlabel, ylabel, event_name)
    ax5.text(0.82, 2.8, 'Corr=' + str(round(Nucl_size_norm_end_corr[0],3)) + '  p value=' + str(round(Nucl_size_norm_end_corr[1],3)) , fontsize=6)

    ax6 = fig.add_subplot(3, 2, 6)
    xaxis = ([0,15000])
    yaxis = ([0.8,3])
    xlabel = "Nucle_size_start"
    ylabel = "Translocation Efficiency"
    rimdata.make_scatter_plot(Nucle_size_start,tran_efficiency_max, ax6, xaxis, yaxis, xlabel, ylabel, event_name)
    ax6.text(50, 2.8, 'Corr=' + str(round(Nucle_size_start_corr[0],3)) + '  p value=' + str(round(Nucl_size_norm_end_corr[1],3)) , fontsize=6)      

    
    fig.subplots_adjust(wspace=0.3, hspace=0.3)
            
#    matplotlib.pylab.show()
    fig.savefig(output_pdf_2, dpi=400)
    print('Proccesing: DONE!')
    fig.clear()
    

    
    

## Generate Figure and place text info
#
#    fig = matplotlib.pylab.figure(1,figsize=(8, 11))
#    fig.suptitle(treatment + " " + well, fontsize=10, fontweight='bold')
#    fig.text(0.52, 0.82, "Mean Translocation Efficiency: " + str(mean_tran_eff),fontsize=12)
#    fig.text(0.52, 0.79, "Events Passed: " + event_passed_tot,fontsize=10)
#
## make plot of translocation efficiency over time with time points as boxplot
#
#    ax1 = fig.add_subplot(3, 2, 1)
#    xaxis = ([0,18])
#    yaxis = ([0.4,2.5])
#    xlabel = "Time Point"
#    ylabel = "Norm_RIM_Intensity"
#    ax1.axhline(1,linewidth=1, color='g', ls = '--')
#    ax1.axhline(mean_tran_eff, linewidth=1, color='g', ls = '--')
#    rim_intensity_max_norm = well_data.rim_intensity_max_norm
#    rimdata.plot_time_series(rim_intensity_max_norm, ax1, xaxis, yaxis, xlabel, ylabel)
#
#        
# # make scatter plots 
#    event_name = well_data.event_name
#    ER_int_start = well_data.ER_inten_start
#    rim_int_start = well_data.rim_intensity_start
#    NE_ER_ratio = well_data.NE_ER_ratio
#    tran_efficiency_max = well_data.tran_efficiency_max
#    
#    ax2 = fig.add_subplot(3, 2, 3)
#    xaxis = ([0,40])
#    yaxis = ([0.8,3])
#    xlabel = "ER_int_start"
#    ylabel = "Translocation Efficiency"
#    rimdata.make_scatter_plot(ER_int_start,tran_efficiency_max, ax2, xaxis, yaxis, xlabel, ylabel, event_name)  
#    
#    ax3 = fig.add_subplot(3, 2, 4)
#    xaxis = ([0,40])
#    yaxis = ([0.8,2])
#    xlabel = "RIM_int_start"
#    ylabel = "NE_ER_ratio_start"
#    rimdata.make_scatter_plot(rim_int_start,NE_ER_ratio, ax3, xaxis, yaxis, xlabel, ylabel, event_name)
#    
#    ax4 = fig.add_subplot(3, 2, 5)
#    xaxis = ([0.4,2])
#    yaxis = ([0.8,3])
#    xlabel = "NE_ER_ratio_start"
#    ylabel = "Translocation Efficiency"
#    rimdata.make_scatter_plot(NE_ER_ratio,tran_efficiency_max, ax4, xaxis, yaxis, xlabel, ylabel, event_name)
#    
#    ax5 = fig.add_subplot(3, 2, 6)
#    xaxis = ([0,18])
#    yaxis = ([0.2,1.2])
#    xlabel = "Time Point"
#    ylabel = "Norm_ER_Intensity"
#    ER_int_norm = well_data.ER_int_norm
#    rimdata.plot_time_series(ER_int_norm, ax5, xaxis, yaxis, xlabel, ylabel)    
#    
#    fig.subplots_adjust(wspace=0.3, hspace=0.3)
#            
##    matplotlib.pylab.show()
#    fig.savefig(output_pdf, dpi=400)
#    print('Proccesing: DONE!')
#    fig.clear()
#    
#
#
#
#
