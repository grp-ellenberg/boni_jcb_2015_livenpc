import rimdata
import pandas
import numpy
import matplotlib.pylab
import os

## Function to run the summary of each labtek
###############################################################

def labtek_summary(labtek_dataset, labteck_id, output_folder):
    
    labtek_dataset = labtek_dataset[labtek_dataset.segment_count_included > 0]
    labtek_dataset = labtek_dataset[labtek_dataset.rim_intensity_max_start > 15]
    
    output_pdf = os.path.join(output_folder,'labtek_summary.eps')
    
    rimdata.plot_labtek_summary(labtek_dataset,output_pdf,labteck_id)
   
