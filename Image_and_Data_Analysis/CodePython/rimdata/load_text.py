import pandas
import numpy


# Load .txt file into a pandas DataFrama

def load_text(text_file):
  
# Functions:

    # Take first row of col_name
    def take_first(df, col_name):
        return df.iloc[1][col_name]
        
    # Separate col_name after "--" into 3 separated string   
    def split_pos_name(df, col_name):
        pos_name = df.iloc[1][col_name]
        index = pos_name.split('--')
        fisttwo = index[0]+'--'+index[1]
        return (fisttwo, index[1], index[2])
    
    # Compute mean of first two value of col_name        
    def mean_of_start(df, col_name):
        return df.iloc[0:2][col_name].mean()
        
    # Extract all value of col_name and place them into nupy array       
    def take_time_series(df, col_name):
        return numpy.array(df[col_name], numpy.float)
     
    # Return mean of seg_included and seg_count if in col_name1 there are no 0 values
    def seg_included(df, col_name1, col_name2):
        if sum(df[col_name1]== 0) == 0:
            seg_included = round((df[col_name1]).mean())
            seg_count = round((df[col_name2]).mean())
            return (seg_included, seg_count)
        else:
            seg_included = 0
            seg_count = round((df[col_name2]).mean())
            return (seg_included, seg_count)


    crs = pandas.io.parsers.read_table(text_file)
     
    # Create event ID unique identifier and add it to the DataFrame 
    crs['event_id'] = crs.experiment_name + '_' + crs.position_name + + crs.event_name
    event_ids = crs.event_id.unique()
    
    final_data = list()
    column_names = list()
    first_iteration = True


    # Iterate over all event inside the DataFrame and generate a list of values for each event 
    for current_event_id in event_ids:
    
        event_dataset = crs[crs.event_id == current_event_id]
        
        final_data_row = list()    
       
    
        if first_iteration: column_names.extend(['labtek_ID','treatment','gene','well','event_name','n_time_point','segment_count_included','segment_count_total'])     
        
        final_data_row.append(take_first(event_dataset, 'experiment_name'))
        
        treatment, gene, well = split_pos_name(event_dataset, "position_name")    
        final_data_row.append(treatment)
        final_data_row.append(gene)
        final_data_row.append(well)
        
        final_data_row.append(take_first(event_dataset, 'event_name'))  
        final_data_row.append(len(event_dataset['event_name']))    # return number of time point
        
        segment_count_included, segment_count = seg_included(event_dataset, 'segment_count_included', 'segment_count')
        final_data_row.append(segment_count_included)
        final_data_row.append(segment_count)
        
        colums_to_means_start = ["rim_intensity", "rim_intensity_max", "cytoplasmic_inten", "nucl_inten", "nucl_size"]   
        if first_iteration: column_names.extend(["rim_intensity_start", "rim_intensity_max_start", "ER_inten_start", "nucl_inten_start","nucle_size_start"])
        for i in colums_to_means_start:
            final_data_row.append(mean_of_start(event_dataset, i))
            
        
        colums_to_time_series = ["rim_intensity", "rim_intensity_max", "cytoplasmic_inten", "nucl_inten", "nucl_size"]   
        if first_iteration: column_names.extend(["rim_intensity","rim_intensity_max","ER_inten","nucl_inten", "nucl_size"])
        for c in colums_to_time_series:
            final_data_row.append(take_time_series(event_dataset, c))
        
        # Append data of each single event into list                
        final_data.append(final_data_row)
        
        first_iteration = False
               
    # import final_data list into DataFrame current_labtek     
    current_labtek = pandas.DataFrame(final_data, columns=column_names)

    return current_labtek     
