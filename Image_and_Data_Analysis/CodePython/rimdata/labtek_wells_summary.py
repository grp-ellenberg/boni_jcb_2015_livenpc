import pandas
import rimdata
import numpy
import os
from matplotlib.backends.backend_pdf import PdfPages
import csv

## Function to run the wells summary for each labtek
###############################################################


def labtek_wells_summary(labtek_dataset, labtec_id, output_folder):
    
    
    well_ids = labtek_dataset.well.unique()    
    for current_well_id in well_ids:
        
        well_dataset = labtek_dataset[labtek_dataset.well == current_well_id]

        tot_event = str(len(well_dataset))
        
        treatment = well_dataset.treatment.iloc[0]
        well_dataset = well_dataset[well_dataset.segment_count_included > 0]
        well_dataset = well_dataset[well_dataset.rim_intensity_max_start > 15]
        event_passed = str(len(well_dataset))
        event_passed_tot = event_passed + " out of " + tot_event
        del well_dataset['well']
        mean_tran_eff = round(well_dataset.tran_efficiency_max.mean(),2)
        output_pdf_1 = os.path.join(output_folder,treatment + "_" + current_well_id + '.pdf')
        output_pdf_2 = os.path.join(output_folder,treatment + "_" + current_well_id + 'scatter_plot' + '.pdf')
        rimdata.plot_well(well_dataset, labtec_id, current_well_id, output_pdf_1, output_pdf_2, event_passed_tot,treatment,mean_tran_eff)
        
        # Saving the data frame in .csv
        output_csv = os.path.join(output_folder,treatment + "_" + current_well_id + '.csv')

          
        data = well_dataset.rim_intensity_max_norm
        data2 = data.append(well_dataset.ER_int_norm)
        data3 = data2.append(well_dataset.NE_ER_ratio_norm)
        data4 = data3.append(well_dataset.rim_intensity_max)
        data5 = data4.append(well_dataset.rim_intensity)
        completedata = data5.append(well_dataset.ER_inten)
        

        time = []
        for i in data:
            time.append([0])        
        
        header  = [];

        for i in data:
            header.append(["rim_intensity_max_norm"])
        for i in data:
            header.append(["ER_int_norm"])
        for i in data:
            header.append(["NE_ER_ratio_norm"])
        for i in data:
            header.append(["rim_intensity_max"])
        for i in data:
            header.append(["rim_intensity"])    
        for i in data:
            header.append(["ER_intensity"])    
       
        zippedheader = zip(*header)
        zippedcompletedata = zip(*completedata)
        
        with open(output_csv, 'w', newline='') as fp:
            a = csv.writer(fp, delimiter=' ')
            a.writerows(zippedheader)
            a.writerows(zippedcompletedata)
       
        
        

             
                