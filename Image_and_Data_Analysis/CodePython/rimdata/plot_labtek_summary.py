import rimdata
import pandas
import numpy
import matplotlib.pylab
import subprocess
import rimdata

## Function to plot the results of labtek 
#################################################################

def plot_labtek_summary(labtek_dataset_raw, labtek_dataset_norm, output_pdf_1,output_pdf_2, current_labteck_id):


#  Generate list of data for each treatment and plot them in the proper subplot   
    
    data_te = list()
    data_ratio = list()
    data_er = list()
    data_er_norm_end = list()
    data_nucl_size_end = list()
    label = list()
                
    for i in labtek_dataset_raw.treatment.unique():
        dataset = labtek_dataset_raw[labtek_dataset_raw.treatment == i]
        number = str(len(dataset))
        data_ratio.append(dataset.NE_ER_ratio)
        data_te.append(dataset.tran_efficiency_max)
        data_er.append(dataset.ER_inten_start)
        data_er_norm_end.append(dataset.ER_norm_end)
        data_nucl_size_end.append(dataset.Nucl_size_norm_end)
        label.append(i + " (n=" + number + ")")
        
        
   #  Generate figure and create axis for subplot 
    

    fig = matplotlib.pylab.figure(1,figsize=(14, 13))
    fig.suptitle(current_labteck_id, fontsize=15, fontweight='bold')    
    ax1 = fig.add_subplot(3,3,1)   
    ax2 = fig.add_subplot(3,3,2) 
    ax3 = fig.add_subplot(3,3,3) 
    ax4 = fig.add_subplot(3,3,4) 
    ax5 = fig.add_subplot(3,3,5)      
        
         
    ax1.boxplot(data_te)
    ax1.set_ylim([0.8,2.8])
    ax1.set_ylabel('Translocation_Efficiency',size='small')
    xtickNames = matplotlib.pylab.setp(ax1, xticklabels = label)  # Check ax1.stp
    matplotlib.pylab.setp(xtickNames, rotation=90, fontsize=7)  # Check ax1.stp
    
    ax2.boxplot(data_ratio)
    ax2.set_ylim([0,2])
    ax2.set_ylabel('NE_ER_ratio_start',size='small')
    xtickNames = matplotlib.pylab.setp(ax2, xticklabels = label)
    matplotlib.pylab.setp(xtickNames, rotation=90, fontsize=7)
    
    ax3.boxplot(data_er)
    xtickNames = matplotlib.pylab.setp(ax3, xticklabels = label)
    ax3.set_ylim([0,50])
    ax3.set_ylabel('ER_Intensity_Start',size='small')
    matplotlib.pylab.setp(xtickNames, rotation=90, fontsize=7)
    
    ax4.boxplot(data_er_norm_end)
    xtickNames = matplotlib.pylab.setp(ax4, xticklabels = label)
    ax4.set_ylim([1.2,0])
    ax4.set_ylabel('ER_Normalized_End',size='small')
    matplotlib.pylab.setp(xtickNames, rotation=90, fontsize=7)    
    
    ax5.boxplot(data_nucl_size_end)
    xtickNames = matplotlib.pylab.setp(ax5, xticklabels = label)
    ax5.set_ylim([0.8,1.5])
    ax5.set_ylabel('Nuclear Size_Norm_End',size='small')
    matplotlib.pylab.setp(xtickNames, rotation=90, fontsize=7)     
    
    
    
    
    
    #  adjust distance of subplot  
    
    fig.subplots_adjust(wspace=0.5, hspace=0.7)

#  Save Figure          

    fig.savefig(output_pdf_1, dpi=400, orientation = 'landscape')
    fig.clear()
    
    
    
#  Call function 'get_diff_mean" that return difference of the mean between Control - treatments
#  and plots them in the proper subplot   
    
       #  Generate figure and create axis for subplot 
    

    fig = matplotlib.pylab.figure(1,figsize=(14, 13))
    fig.suptitle(current_labteck_id, fontsize=15, fontweight='bold')    
    ax1 = fig.add_subplot(3,3,1)   
    ax2 = fig.add_subplot(3,3,2) 
    ax3 = fig.add_subplot(3,3,3) 
    ax4 = fig.add_subplot(3,3,4) 
    
    
#   
    (data_TE, label_TE, sign_TE, data_ratio, label_ratio, sign_ratio, data_er_start_sort, label_er_start_ordered, sig_er_start_ordered, data_er_end_sort, label_er_end_ordered, sig_er_end_ordered) = rimdata.get_diff_mean(labtek_dataset_norm)
    
    ind =  numpy.arange(len(label_TE))
    width = 0.70
    ax1.axhline(linewidth=1, color='k')
    ax1.bar(ind+1,data_TE, width)
    ax1.spines['left'].set_linewidth(2)
    ax1.spines['bottom'].set_linewidth(2)
    ax1.spines['right'].set_color('none')
    ax1.spines['top'].set_color('none')
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')
    ax1.set_ylim([-1,1])
    ax1.set_xticks(ind+1+(width/2))
    ax1.set_xticklabels(label_TE, rotation = 90, size='xx-small' )
    ax1.set_ylabel('Deviation from Control Trans.Efficiency',size='small')
    ax1.text(6, 0.8, "* p <= 0.01",fontsize=10)
    for ast in range(len(sign_TE)):
        if data_TE[ast] > 0:
            ax1.text(ind[ast]+1.20,data_TE[ast], sign_TE[ast],fontsize=14)  
        else:
            ax1.text(ind[ast]+1.20,0.05, sign_TE[ast],fontsize=14)   
    
    
    ind =  numpy.arange(len(label_ratio))
    width = 0.70
    ax2.axhline(linewidth=1, color='k')
    ax2.bar(ind+1,data_ratio, width)
    ax2.spines['left'].set_linewidth(2)
    ax2.spines['bottom'].set_linewidth(2)
    ax2.spines['right'].set_color('none')
    ax2.spines['top'].set_color('none')
    ax2.xaxis.set_ticks_position('bottom')
    ax2.yaxis.set_ticks_position('left')
    ax2.set_ylim([-1,1])
    ax2.set_xticks(ind+1+(width/2))
    ax2.set_xticklabels(label_ratio, rotation = 90, size='xx-small' )
    ax2.set_ylabel('Deviation from Control NE_ER_ratio',size='small')
    ax2.text(6, 0.8, "* p <= 0.01",fontsize=10)
    for ast in range(len(sign_TE)):
        if data_ratio[ast] > 0:
            ax2.text(ind[ast]+1.20,data_ratio[ast], sign_ratio[ast],fontsize=14)  
        else:
            ax2.text(ind[ast]+1.20,0.05, sign_ratio[ast],fontsize=14)
    
    
    ind =  numpy.arange(len(label_er_start_ordered))
    width = 0.70
    ax3.axhline(linewidth=1, color='k')
    ax3.bar(ind+1,data_er_start_sort, width)
    ax3.spines['left'].set_linewidth(2)
    ax3.spines['bottom'].set_linewidth(2)
    ax3.spines['right'].set_color('none')
    ax3.spines['top'].set_color('none')
    ax3.xaxis.set_ticks_position('bottom')
    ax3.yaxis.set_ticks_position('left')
    ax3.set_ylim([-1,1])
    ax3.set_xticks(ind+1+(width/2))
    ax3.set_xticklabels(label_er_start_ordered, rotation = 90, size='xx-small' )
    ax3.set_ylabel('Deviation from Control ER Start',size='small')
    ax3.text(6, 0.8, "* p <= 0.01",fontsize=10)
    for ast in range(len(sig_er_start_ordered)):
        if data_er_start_sort[ast] > 0:
            ax3.text(ind[ast]+1.20,data_er_start_sort[ast], sig_er_start_ordered[ast],fontsize=14)  
        else:
            ax3.text(ind[ast]+1.20,0.05, sig_er_start_ordered[ast],fontsize=14) 
            
            
    ind =  numpy.arange(len(label_er_end_ordered))
    width = 0.70
    ax4.axhline(linewidth=1, color='k')
    ax4.bar(ind+1,data_er_end_sort, width)
    ax4.spines['left'].set_linewidth(2)
    ax4.spines['bottom'].set_linewidth(2)
    ax4.spines['right'].set_color('none')
    ax4.spines['top'].set_color('none')
    ax4.xaxis.set_ticks_position('bottom')
    ax4.yaxis.set_ticks_position('left')
    ax4.set_ylim([-1,1])
    ax4.set_xticks(ind+1+(width/2))
    ax4.set_xticklabels(label_er_end_ordered, rotation = 90, size='xx-small' )
    ax4.set_ylabel('Deviation from Control ER Norm. End',size='small')
    ax4.text(6, 0.8, "* p <= 0.01",fontsize=10)
    for ast in range(len(sig_er_end_ordered)):
        if data_er_end_sort[ast] > 0:
            ax4.text(ind[ast]+1.20,data_er_end_sort[ast], sig_er_end_ordered[ast],fontsize=14)  
        else:
            ax4.text(ind[ast]+1.20,0.05, sig_er_end_ordered[ast],fontsize=14)    
            
    
    
#  adjust distance of subplot  
    
    fig.subplots_adjust(wspace=0.5, hspace=0.7)

#  Save Figure          

    fig.savefig(output_pdf_2, dpi=400, orientation = 'landscape')
    fig.clear()

   
