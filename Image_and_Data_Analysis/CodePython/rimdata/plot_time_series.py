import rimdata
import pandas
import numpy
import matplotlib.pylab

def plot_time_series(data, axis, xaxis, yaxis, xlabel, ylabel):
    
        if len(data) == 1:
            axis.plot(data.iloc[0])
            axis.set_xlim(xaxis)
            axis.set_ylim(yaxis)
            axis.set_xlabel(xlabel)
            axis.set_ylabel(ylabel)
        
        elif len(data) == 0:
            print("porca miseria no events")
    
        else:

            v = [i[numpy.newaxis, :] for i in data]
            total = numpy.concatenate(v, axis=0)
            axis.boxplot(total, sym='', whis=1)
            axis.set_xlim(xaxis)
            axis.set_ylim(yaxis)
            axis.set_xticklabels(range(1,18), size='xx-small')
            axis.set_xlabel(xlabel)
            axis.set_ylabel(ylabel)