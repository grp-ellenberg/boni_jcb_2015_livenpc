import rimdata
import pandas
import numpy
import matplotlib.pylab
import csv
import os

def plot_screen_2(TreatmentDataFrame, screen_output, folder_screen):
    
    fig = matplotlib.pylab.figure(1,figsize=(14, 13))
    ax1 = fig.add_subplot(4,1,1)
    ax2 = fig.add_subplot(4,1,2)
    ax3 = fig.add_subplot(4,1,3)
    ax4 = fig.add_subplot(4,1,4)          
         
    
    treatment_ids = TreatmentDataFrame.treatment.unique()
    print(TreatmentDataFrame)
    
    te = list()
    ratio = list()
    label= list()
    label_te = list()
    label_ratio = list()
    label_er = list()
    laber_er_norm_end = list()
    er_start = list()
    er_norm_end = list()
    stat_te = list()
    stat_ratio = list()
    stat_er = list()
    stat_er_norm_end = list()
    event_cum = list()
    treatment = list()
    number_labtek = list()
    for current_treatment_ids in treatment_ids:
        
        if current_treatment_ids != "s8100--LBR":
        
        
            Curr_treat_DF = TreatmentDataFrame[TreatmentDataFrame.treatment == current_treatment_ids]
            
            
            print(current_treatment_ids)
            te.append((Curr_treat_DF.TE_diff_norm.mean()))
            ratio.append((-1)*(Curr_treat_DF.NE_ER_diff.mean()))
            label.append(current_treatment_ids)
            er_start.append((Curr_treat_DF.ER_start_diff.mean()))
            er_norm_end.append((-1)*(Curr_treat_DF.ER_end_diff.mean()))
            pvalue_te = Curr_treat_DF.TE_ttest
            pvalue_ratio = Curr_treat_DF.NE_ER_ttest
            pvalue_er = Curr_treat_DF.ER_start_ttest
            pvalue_er_end = Curr_treat_DF.ER_end_ttest
            event_cum.append(Curr_treat_DF.num_event.sum()/len(Curr_treat_DF))
            treatment.append(current_treatment_ids)
            number_labtek.append(str(len(Curr_treat_DF)))      
            
            
            
        
            nsign_te = sum(p <= 0.01 for p in pvalue_te)
            
            
            if nsign_te == 2:
                stat_te.append("*")
                label_te.append("* " + current_treatment_ids + " (" + str(len(Curr_treat_DF)) +")")
            elif nsign_te >= 3:
                stat_te.append("**")
                label_te.append("** " +current_treatment_ids + " (" + str(len(Curr_treat_DF)) +")")
            else:
                stat_te.append(" ")
                label_te.append(current_treatment_ids + " (" + str(len(Curr_treat_DF)) +")")
            
            nsign_ratio = sum(p <= 0.01 for p in pvalue_ratio)
            
            if nsign_ratio == 2:
                stat_ratio.append("*")
                label_ratio.append("* " + current_treatment_ids + " (" + str(len(Curr_treat_DF)) +")")
            elif nsign_ratio >= 3:
                stat_ratio.append("**")
                label_ratio.append("** " +current_treatment_ids + " (" + str(len(Curr_treat_DF)) +")")
            else:
                stat_ratio.append(" ")
                label_ratio.append(current_treatment_ids + " (" + str(len(Curr_treat_DF)) +")")
                
            nsign_er = sum(p <= 0.01 for p in pvalue_er)
            
            if nsign_er == 2:
                stat_er.append("*")
                label_er.append("* " + current_treatment_ids + " (" + str(len(Curr_treat_DF)) +")")
            elif nsign_er >= 3:
                stat_er.append("**")
                label_er.append("** " +current_treatment_ids + " (" + str(len(Curr_treat_DF)) +")")
            else:
                stat_er.append(" ")
                label_er.append(current_treatment_ids + " (" + str(len(Curr_treat_DF)) +")")
                
                
            nsign_er_norm_end = sum(p <= 0.01 for p in pvalue_er_end)
            
            if nsign_er_norm_end == 2:
                stat_er_norm_end.append("*")
                laber_er_norm_end.append("* " + current_treatment_ids + " (" + str(len(Curr_treat_DF)) +")")
            elif nsign_er_norm_end >= 3:
                stat_er_norm_end.append("**")
                laber_er_norm_end.append("** " +current_treatment_ids + " (" + str(len(Curr_treat_DF)) +")")
            else:
                stat_er_norm_end.append(" ")
                laber_er_norm_end.append(current_treatment_ids + " (" + str(len(Curr_treat_DF)) +")")    
    
    
    te_sort = sorted(te, reverse=True)
    te_enumerate= sorted(enumerate(te), key=lambda tup: tup[1], reverse=True)
    node_order = [e[0] for e in te_enumerate] 
    label_ordered_te = list()
    stat_ordered_te = list()
    for i in range(0, len(label_te)):
        label_ordered_te.append(label_te[node_order[i]] )   
    for i in range(0, len(stat_te)):
        stat_ordered_te.append(stat_te[node_order[i]] )      
    

    ratio_sort = sorted(ratio, reverse=True)
    ratio_enumerate= sorted(enumerate(ratio), key=lambda tup: tup[1], reverse=True)
    node_order_2 = [e[0] for e in ratio_enumerate]
    label_ordered_ratio = list()
    stat_ordered_ratio = list()
    for i in range(0, len(label_ratio)):
        label_ordered_ratio.append(label_ratio[node_order_2[i]] ) 
    for i in range(0, len(stat_ratio)):
        stat_ordered_ratio.append(stat_ratio[node_order_2[i]] )
        
    er_start_ordered = list()
    er_start_ordered_label = list()
    er_start_ordered_stat = list()
    for i in range(0, len(er_start)):
        er_start_ordered.append(er_start[node_order[i]] )   
    for i in range(0, len(label_er)):
        er_start_ordered_label.append(label_er[node_order[i]] )
    for i in range(0, len(stat_er)):
        er_start_ordered_stat.append(stat_er[node_order[i]] )
        
        
    er_end_ordered = list()
    er_end_ordered_label = list()
    er_end_ordered_stat = list()
    for i in range(0, len(er_norm_end)):
        er_end_ordered.append(er_norm_end[node_order[i]] )   
    for i in range(0, len(label_er)):
        er_end_ordered_label.append(laber_er_norm_end[node_order[i]] )
    for i in range(0, len(stat_er)):
        er_end_ordered_stat.append(stat_er_norm_end[node_order[i]] )
        
    event_cum_order= list()
    for i in range(0, len(event_cum)):
        event_cum_order.append(event_cum[node_order[i]] )         
        
#    er_end_ordered = sorted(er_norm_end, reverse=True)
#    er_end_ordered_enumerate = sorted(enumerate(er_norm_end), key=lambda tup: tup[1], reverse=True)
#    node_order_3 = [e[0] for e in er_end_ordered_enumerate]
#    er_end_ordered_stat = list()
#    er_end_ordered_label = list()
#    for i in range(0, len(label_er)):
#        er_end_ordered_label.append(laber_er_norm_end[node_order_3[i]] )
#    for i in range(0, len(stat_er)):
#        er_end_ordered_stat.append(stat_er_norm_end[node_order_3[i]] )
#          
        
        
    event_cum_order= list()
    for i in range(0, len(event_cum)):
        event_cum_order.append(event_cum[node_order[i]] ) 
    
        
    
    
     
    ind =  numpy.arange(len(label_ordered_te))
    width = 0.70
    ax1.bar(ind+1,te_sort,width)
    ax1.set_xticks(ind+1+(width/2))
    ax1.set_xticklabels(label_ordered_te, rotation = 90, fontsize=4)
    ax1.set_ylim([-1.2,1.2])
    ax1.axhline(linewidth=1, color='k')
    ax1.set_ylabel('Deviation from Control Trans.Efficiency',size='small')
    for ast in range(len(stat_ordered_te)):
        if te_sort[ast] > 0:
            ax1.text(ind[ast]+1.1,te_sort[ast], stat_ordered_te[ast],fontsize=5)  
        else:
            ax1.text(ind[ast]+1.1,te_sort[ast]-0.1, stat_ordered_te[ast],fontsize=5) 
        
    ind =  numpy.arange(len(label_ordered_ratio))
    ax2.bar(ind+1,ratio_sort,width)
    ax2.set_xticks(ind+1+(width/2))
    ax2.set_xticklabels(label_ordered_ratio, rotation = 90, fontsize=4)
    ax2.set_ylim([-1.5,1.5])
    ax2.axhline(linewidth=1, color='k')
    ax2.set_ylabel('Deviation from Control NE_ER_ratio',size='small')
    for ast in range(len(stat_ordered_ratio)):
        if ratio_sort[ast] > 0:
            ax2.text(ind[ast]+1.1,ratio_sort[ast], stat_ordered_ratio[ast],fontsize=5)  
        else:
            ax2.text(ind[ast]+1.1,ratio_sort[ast]-0.1, stat_ordered_ratio[ast],fontsize=5)
            
            
        
#    ind =  numpy.arange(len(er_end_ordered_label))
#    ax2.bar(ind+1,er_end_ordered,width)
#    ax2.set_xticks(ind+1+(width/2))
#    ax2.set_xticklabels(er_end_ordered_label, rotation = 90, fontsize=4)
#    ax2.set_ylim([-0.5,0.5])
#    ax2.axhline(linewidth=1, color='k')
#    ax2.set_ylabel('Deviation from Control ER End',size='small')
#    for ast in range(len(er_end_ordered_stat)):
#        if er_end_ordered[ast] > 0:
#            ax2.text(ind[ast]+1.1,er_end_ordered[ast], er_end_ordered_stat[ast],fontsize=5)  
#        else:
#            ax2.text(ind[ast]+1.1,er_end_ordered[ast]-0.1, er_end_ordered_stat[ast],fontsize=5)                
    
    
    ind =  numpy.arange(len(er_start_ordered_label))
    ax3.bar(ind+1,er_start_ordered,width)
    ax3.set_xticks(ind+1+(width/2))
    ax3.set_xticklabels(er_start_ordered_label, rotation = 90, fontsize=4)
    ax3.set_ylim([-1.2,1.2])
    ax3.axhline(linewidth=1, color='k')
    ax3.set_ylabel('Deviation from Control ER Start',size='small')
    for ast in range(len(er_start_ordered_stat)):
        if er_start_ordered[ast] > 0:
            ax3.text(ind[ast]+1.1,er_start_ordered[ast], er_start_ordered_stat[ast],fontsize=5)  
        else:
            ax3.text(ind[ast]+1.1,er_start_ordered[ast]-2, er_start_ordered_stat[ast],fontsize=5)
            
            
    
    
    
    ind =  numpy.arange(len(event_cum))
    ax4.bar(ind+1,event_cum_order,width)
    ax4.set_xticks(ind+1+(width/2))
    ax4.set_xticklabels(er_start_ordered_label, rotation = 90, fontsize=10)
    #ax4.set_ylim([-20,20])
    ax4.axhline(linewidth=1, color='k')
    ax4.set_ylabel('Number of cells/replicate',size='small')
      
 
    
    fig.subplots_adjust(wspace=0.5, hspace=0.7)
#    matplotlib.pylab.show()
    
    fig.savefig(screen_output, dpi=400, orientation = 'landscape')
    fig.clear()
    
    
    
     # Saving the data frame in .csv
    output_csv = os.path.join(folder_screen, 'ScreenDataAnalysis.csv')

    data = list()
    data.append(treatment)
    data.append(number_labtek)
    data.append(te)
    data.append(ratio)
    data.append(er_start)
    data.append(er_norm_end)         
          
    time = []
    for i in data:
        time.append([0])        
        
    header  = (["Treatment"],["Number_Labtek"],["Dev.Trans.Efficiency"],["Dev.Ratio"],["Dev.Er_Start"],["Dev.ER_End"]);

    zippedheader = zip(*header)
    zippedcompletedata = zip(*data)
       

    with open(output_csv, 'w', newline='') as fp:
        a = csv.writer(fp, delimiter=',')
        a.writerows(zippedheader)
        a.writerows(zippedcompletedata)
        