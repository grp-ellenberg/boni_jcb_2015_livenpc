import glob
import rimdata
import pandas
import numpy
import os
import scipy.stats
import matplotlib.pylab
import csv
## Function to plot the Negative Control of all the screen

def plot_NegControl(ScreenDataFrame,NegCon_screen_output,folder_screen):
    
    labtek_ids = ScreenDataFrame.labtek_ID.unique()      
    NegControlDataFrame =   ScreenDataFrame[ScreenDataFrame.treatment == "XWNeg9--XWNeg9"]
    neg_te = list()
    neg_ratio = list()
    neg_ER_start = list()
    neg_event_num = list()
    label = list()
    TE_export = list()
    NE_ER_Ratio = list()
    ER_start = list()
    for current_labteck_id in labtek_ids:

        NegControlDataFrame_current =   NegControlDataFrame[NegControlDataFrame.labtek_ID == current_labteck_id]
        neg_te.append(numpy.array(NegControlDataFrame_current["tran_efficiency_max"], numpy.float))
        neg_ratio.append(numpy.array(NegControlDataFrame_current["NE_ER_ratio"], numpy.float))
        neg_ER_start.append(numpy.array(NegControlDataFrame_current["ER_inten_start"], numpy.float))
        neg_event_num.append(len(NegControlDataFrame_current))
        label.append(current_labteck_id)
        TE_export.append(numpy.mean(numpy.array(NegControlDataFrame_current["tran_efficiency_max"], numpy.float)))
        NE_ER_Ratio.append(numpy.mean(numpy.array(NegControlDataFrame_current["NE_ER_ratio"], numpy.float)))
        ER_start.append(numpy.mean(numpy.array(NegControlDataFrame_current["ER_inten_start"], numpy.float)))
        
        

    fig = matplotlib.pylab.figure(1,figsize=(14, 13))
    fig.suptitle("Negative Control (XWNeg9) across the screen")
    ax1 = fig.add_subplot(4,1,1)
    ax1.set_ylabel('Translocation Efficiency',size='small')
    ax2 = fig.add_subplot(4,1,2) 
    ax2.set_ylabel('NE_ER_Ratio_Start',size='small')
    ax3 = fig.add_subplot(4,1,3)
    ax3.set_ylabel('ER_Int_Start',size='small')
    ax3.set_ylim([0,60])
    ax4 = fig.add_subplot(4,1,4) 
    ax4.set_ylabel('Number of Events (cells)',size='small')
    
    ax1.boxplot(neg_te)
    ax2.boxplot(neg_ratio)
    ax3.boxplot(neg_ER_start)
    
    ind =  range(len(neg_event_num))
    width = 0.60   
    ax4.bar(ind,neg_event_num, width, align='center')
    ax4.set_xlim([-0.5,(len(neg_event_num)-0.5)])
    ax4.set_xticks(ind)
    ax4.set_xticklabels(label,fontsize=6, rotation = 80)
    fig.autofmt_xdate()
    

    
     # Saving the data frame in .csv
    output_csv = os.path.join(folder_screen, 'ScreenDataAnalysis_Control.csv')
    
    data = list()
    data.append(TE_export)
    data.append(NE_ER_Ratio)
    data.append(ER_start)
    
      
          
    time = []
    for i in data:
        time.append([0])        
        
    header  = (["Trans.Efficiency"],["NE_ER_Ratio"],["ER_Int_Start"]);

    zippedheader = zip(*header)
    zippedcompletedata = zip(*data)
       

    with open(output_csv, 'w', newline='') as fp:
        a = csv.writer(fp, delimiter=',')
        a.writerows(zippedheader)
        a.writerows(zippedcompletedata)
            
    