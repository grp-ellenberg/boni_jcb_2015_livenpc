import pandas
import rimdata
import numpy
import os

# Return Normalized RIM intensity to rim intensity start and mean 3 highest consecutive Normalized RIM intensity value
       
def get_tran_eff(dataset, col_name1, col_name2):
    tran_efficiency = numpy.zeros((len(dataset),), dtype=numpy.float)
    rim_nor = numpy.array(dataset[col_name1]/dataset[col_name2])
        
    for i in range(len(dataset)):
        rim_nor_max = 0
        for c in range(3 , len(rim_nor[i])-1):
            rim_nor_max = max(rim_nor_max, numpy.mean(rim_nor[i][c-1:c+2]))
        tran_efficiency[i] = rim_nor_max
   
    return (rim_nor, tran_efficiency)    
        
# Return NE_ER ratio        
        
def get_NE_ERratio(dataset, col_name1, col_name2):
        
    NE_ERratio = numpy.array(dataset[col_name1]/dataset[col_name2])
    return NE_ERratio
    
# Return Normalized ER intesity to ER intesity start
    
def ER_norm(dataset, col_name1, col_name2):
    
    ER_nor = numpy.array(dataset[col_name1]/dataset[col_name2])
    return ER_nor
    
def take_end_value(dataset, col_name1, col_name2):
    
    dataset_nor = numpy.array(dataset[col_name1]/dataset[col_name2])
    end_value = numpy.zeros((len(dataset),), dtype=numpy.float)
    
    for i in range(len(dataset)):    
        c = len(dataset_nor[i])
        end_value[i] = numpy.mean(dataset_nor[i][c-2:c])
        
    return end_value