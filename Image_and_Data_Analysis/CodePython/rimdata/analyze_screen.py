import glob
import rimdata
import pandas
import numpy
import os
import scipy.stats
import matplotlib.pylab

# MAIN FUNCTION to analyze the screen

# Loop into input_folder and call the "load_text" function and generate a pandas DataFrame fo the whole Screen

def analyze_screen(input_folder, output_folder):

    first_file = True
    for text_file in glob.glob(input_folder):
        
        current_labtek = rimdata.load_text(text_file)
        
        if first_file:
            ScreenDataFrame = current_labtek.copy()
            first_file = False
        else:
            ScreenDataFrame = pandas.concat([ScreenDataFrame, current_labtek], ignore_index=True)
            
   
    ScreenDataFrame['NE_ER_ratio']=  rimdata.get_NE_ERratio(ScreenDataFrame, "rim_intensity_start", "ER_inten_start")      
    ScreenDataFrame['rim_intensity_max_norm'], ScreenDataFrame['tran_efficiency_max'] =  rimdata.get_tran_eff(ScreenDataFrame, "rim_intensity_max", "rim_intensity_max_start")            
    ScreenDataFrame['ER_int_norm']=  rimdata.ER_norm(ScreenDataFrame, "ER_inten", "ER_inten_start")
    ScreenDataFrame['Nucl_size_norm']=  rimdata.ER_norm(ScreenDataFrame, "nucl_size", "nucle_size_start")
    ScreenDataFrame['ER_norm_end']=  rimdata.take_end_value(ScreenDataFrame, "ER_inten","ER_inten_start")
    ScreenDataFrame['Nucl_size_norm_end']=  rimdata.take_end_value(ScreenDataFrame, "nucl_size","nucle_size_start")
    ScreenDataFrame['NE_ER_ratio_norm'], ScreenDataFrame['tran_efficiency_NE_ER_ratio'] =  rimdata.get_tran_eff(ScreenDataFrame, "rim_intensity_max_norm", "ER_int_norm")
    

    labtek_ids = ScreenDataFrame.labtek_ID.unique()  
#   
    for current_labteck_id in labtek_ids:
        labtek_dataset = ScreenDataFrame[ScreenDataFrame.labtek_ID == current_labteck_id]
        del labtek_dataset["labtek_ID"]
       
        folder_wells_summary = os.path.join(output_folder, current_labteck_id + "_ wells_summary")
        if not os.path.exists(folder_wells_summary): os.makedirs(folder_wells_summary)
            
        # function call for labtek_wells_summary  
#        rimdata.labtek_wells_summary(labtek_dataset, current_labteck_id, folder_wells_summary)
        
    
    ScreenDataFrame = ScreenDataFrame[ScreenDataFrame.segment_count_included > 0]
    ScreenDataFrame = ScreenDataFrame[ScreenDataFrame.rim_intensity_max_start > 15]
      
    

    
    
    first_labtek = True    
    for current_labteck_id in labtek_ids:
        labtek_dataset = ScreenDataFrame[ScreenDataFrame.labtek_ID == current_labteck_id]
        
        
               
        treatment_ids = labtek_dataset.treatment.unique()  
        
        final_data = list()
        column_names = list()        
        control_TE = labtek_dataset.tran_efficiency_max[labtek_dataset.treatment == 'XWNeg9--XWNeg9']
        control_ratio = labtek_dataset.NE_ER_ratio[labtek_dataset.treatment == 'XWNeg9--XWNeg9']
        control_ER_start = labtek_dataset.ER_inten_start[labtek_dataset.treatment == 'XWNeg9--XWNeg9']
        control_ER_norm_end = labtek_dataset.ER_norm_end[labtek_dataset.treatment == 'XWNeg9--XWNeg9']
        
        first_treatment = True  
        
        for current_treatment_id in treatment_ids:
            treatment_dataset = labtek_dataset[labtek_dataset.treatment == current_treatment_id]

 
            if first_treatment: column_names.extend(['treatment','gene','labtek_ID','TE_norm','TE_diff','TE_diff_norm','TE_ttest','NE_ER_Ratio','NE_ER_diff','NE_ER_ttest','ER_start','ER_start_diff','ER_start_ttest','ER_norm_end','ER_end_diff','ER_end_ttest','num_event'])         
       
            final_data_row = list()
            final_data_row.append(treatment_dataset.iloc[0]['treatment'])
            final_data_row.append(treatment_dataset.iloc[0]['gene'])
            final_data_row.append(treatment_dataset.iloc[0]['labtek_ID'])
            final_data_row.append(round(treatment_dataset.tran_efficiency_max.mean(),2))
            diff_TE = (control_TE.mean()) - (treatment_dataset['tran_efficiency_max'].mean())
            diff_TE_norm = (((treatment_dataset['tran_efficiency_max'].mean()-1)  - (control_TE.mean()-1))/(control_TE.mean()-1))
            ttest_TE = scipy.stats.ttest_ind(control_TE,treatment_dataset['tran_efficiency_max'])
            final_data_row.append(diff_TE)
            final_data_row.append(diff_TE_norm)
            final_data_row.append(ttest_TE[1])
            
            final_data_row.append(round(treatment_dataset.NE_ER_ratio.mean(),2))
            diff_ratio = (control_ratio.mean()) - (treatment_dataset['NE_ER_ratio'].mean())
            ttest_ratio = scipy.stats.ttest_ind(control_ratio,treatment_dataset['NE_ER_ratio'])
            final_data_row.append(diff_ratio)
            final_data_row.append(ttest_ratio[1])
            
            final_data_row.append(round(treatment_dataset.ER_inten_start.mean(),2))
            diff_ER_start = ((treatment_dataset['ER_inten_start'].mean()- control_ER_start.mean())/(control_ER_start.mean()-1))

            ttest_ER_start = scipy.stats.ttest_ind(control_ER_start,treatment_dataset['ER_inten_start'])
            final_data_row.append(diff_ER_start)
            final_data_row.append(ttest_ER_start[1])
            
            final_data_row.append(round(treatment_dataset.ER_norm_end.mean(),2))
            diff_ER_norm_end = (control_ER_norm_end.mean()) - (treatment_dataset['ER_norm_end'].mean())
            ttest_ER_norm_end = scipy.stats.ttest_ind(control_ER_norm_end,treatment_dataset['ER_norm_end'])
            final_data_row.append(diff_ER_norm_end)
            final_data_row.append(ttest_ER_norm_end[1])            
            
            
            final_data_row.append(len(treatment_dataset))
            
            final_data.append(final_data_row)
            first_treatment = False
            current_labtek = pandas.DataFrame(final_data, columns=column_names)
        
        if first_labtek:
            TreatmentDataFrame = current_labtek.copy()
            first_labtek = False
        else:
            TreatmentDataFrame = pandas.concat([TreatmentDataFrame, current_labtek], ignore_index=True)
            

        
        

# From the ScreenDataFrame extracts DataFrame for each labtek
    for current_labteck_id in labtek_ids:
        labtek_dataset_raw = ScreenDataFrame[ScreenDataFrame.labtek_ID == current_labteck_id]
        labtek_dataset_norm = TreatmentDataFrame[TreatmentDataFrame.labtek_ID == current_labteck_id]
        folder_labteck_summary = os.path.join(output_folder, current_labteck_id + "_labtek_summary")
        if not os.path.exists(folder_labteck_summary): os.makedirs(folder_labteck_summary)
        output_pdf_1 = os.path.join(folder_labteck_summary,'labtek_summary_boxplot.pdf')
        output_pdf_2 = os.path.join(folder_labteck_summary,'labtek_summary_deviation.pdf')
        
        # function call for labtek_summary 
#        rimdata.plot_labtek_summary(labtek_dataset_raw, labtek_dataset_norm, output_pdf_1, output_pdf_2 ,current_labteck_id)
    

#   Function call for plot_NegControl
    
    folder_screen = os.path.join(output_folder, "screen_plot")
    if not os.path.exists(folder_screen): os.makedirs(folder_screen)
###    
#    NegCon_screen_output = os.path.join(folder_screen,'plot_NegControl.pdf')    
#    rimdata.plot_NegControl(ScreenDataFrame,NegCon_screen_output,folder_screen)

#   Function call for plot_screen
    
####    
    screen_output = os.path.join(folder_screen,'plot_screen_1.pdf')    
    rimdata.plot_screen_2(TreatmentDataFrame, screen_output, folder_screen)
##    
#
###        
