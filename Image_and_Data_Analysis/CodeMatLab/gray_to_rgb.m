% convert grayscale image to RGB without autoscaling
function image_out = gray_to_rgb(image_in)
    
    if isa(image_in, 'uint8')
        image_out = double(image_in) / (2^8-1);
        image_out = cat(3, image_out, image_out, image_out);
    elseif isa(image_in, 'uint16')
        image_out = double(image_in) / (2^16-1);
        image_out = cat(3, image_out, image_out, image_out);
    elseif isa(image_in, 'double')
        image_out = cat(3, image_in, image_in, image_in);
    end    
    
end