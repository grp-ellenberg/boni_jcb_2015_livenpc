function [data, previews] = quantify_rim_image(image_gfp, image_region, image_region_all)

    % segment size settings
    step_length = 10; length_nucl = 16; length_cytopl = 32; % step lenght original 10
    % rim search reagion in pixels, conditions: length_cytopl - rim_search_cytopl must be >= 5
    %                                           length_nucl - rim_search_nucl must be >= 6
    rim_search_nucl = 10; rim_search_cytopl = 2;
    % step size for derivate in the rim, keep at 1
    diff_step = 1;
    % maximum of first derivate detection criteria
    min_area = 2; min_integr_int = 4;        % original = min_area = 2; min_integr_int = 4;
    
    % determine nuclear size
    nuclear_size = nnz(image_region);
    
    % smoothen the nucleus and trace the border
    image_region = imdilate(image_region, strel('diamond', 2));
    trace = bwboundaries(image_region, 8 ,'noholes'); trace = trace{1};
    
    % remove theevent cell and keep the cells around
    image_region_compl = imcomplement(image_region);
    image_other = immultiply(image_region_all,image_region_compl);

    % calculate steps along the nuclear envelope with a specified distance
    segment_count = 0; current_segment_start = trace(1,:);
    for i = 1:size(trace,1)
        if norm(current_segment_start - trace(i,:)) >= step_length
            segment_count = segment_count + 1;
            segment_starts(segment_count, :) = current_segment_start; %#ok
            segment_ends(segment_count, :) = trace(i,:); %#ok
            current_segment_start = trace(i,:);
        end
    end
        
    % iterate through segments and generate new coordinate system and select valid segments
    % that can be analysed (region does not cross image border)
    
    segment_valid = ones(segment_count, 1);    
    for i = 1:segment_count
        
        % calculate new coodrinate system axes
        segment_vector_x(i,:) = segment_ends(i,:) - segment_starts(i,:);
        segment_vector_x(i,:) = segment_vector_x(i,:) / norm(segment_vector_x(i,:));
        segment_vector_y(i,:) = [-segment_vector_x(i,2), segment_vector_x(i,1)];
        origin(i,:) = segment_starts(i,:) - segment_vector_y(i,:)*length_nucl;
        
        % calculate corner coordinates            
        segment_c1(i,:) = origin(i,:);
        segment_c2(i,:) = origin(i,:) + segment_vector_x(i,:)*step_length + segment_vector_y(i,:)*(length_nucl+length_cytopl);
        segment_c3(i,:) = origin(i,:) + segment_vector_x(i,:)*step_length;
        segment_c4(i,:) = origin(i,:) + segment_vector_y(i,:)*(length_nucl+length_cytopl);

        % round corner coordinates
        rounded_coord_1 = round(cat(1, segment_c1(i, 1), segment_c2(i, 1), segment_c3(i, 1), segment_c4(i, 1)));
        rounded_coord_2 = round(cat(1, segment_c1(i, 2), segment_c2(i, 2), segment_c3(i, 2), segment_c4(i, 2)));

        % make sure the corner coordinates are inside the cropped image, if not exclude the segment from analysis
        if sum(rounded_coord_1 < 1) > 0 || sum(rounded_coord_2 < 1) > 0 || ...
           sum(rounded_coord_1 > size(image_gfp, 1)) > 0 || sum(rounded_coord_2 > size(image_gfp, 2)) > 0
            segment_valid(i) = 0;
        end
    end

    % allocate memory for projections
    profiles = zeros(length_nucl+length_cytopl, segment_count, 'double');
    counts = zeros(length_nucl+length_cytopl, segment_count, 'double');    
    profiles_other = zeros(length_nucl+length_cytopl, segment_count, 'double');
    
    % iterate through segments and generate integrated profile for each valid one
    for i = 1:segment_count
        if segment_valid(i)
            jmin = round(min([segment_c1(i, 1) segment_c2(i, 1) segment_c3(i, 1) segment_c4(i, 1)]));
            jmax = round(max([segment_c1(i, 1) segment_c2(i, 1) segment_c3(i, 1) segment_c4(i, 1)]));
            kmin = round(min([segment_c1(i, 2) segment_c2(i, 2) segment_c3(i, 2) segment_c4(i, 2)]));
            kmax = round(max([segment_c1(i, 2) segment_c2(i, 2) segment_c3(i, 2) segment_c4(i, 2)]));
            for j = jmin:jmax                
                for k = kmin:kmax
                    % calculat x pixel coordinate
                    pixel_x = round(dot([j k] - origin(i, :), segment_vector_x(i, :)));
                    % calculat y pixel coordinate
                    pixel_y = round(dot([j k] - origin(i, :), segment_vector_y(i, :)));
                    if pixel_x > 0 && pixel_x <= step_length && pixel_y > 0 && pixel_y <= (length_nucl+length_cytopl) 
                        profiles(pixel_y, i) = profiles(pixel_y, i) + double(image_gfp(j,k));
                        profiles_other(pixel_y, i) = profiles_other(pixel_y, i) + double(image_other(j,k));
                        counts(pixel_y, i) = counts(pixel_y, i) + 1;
                    end                
                end
            end
        end
    end

    % calculate average profilee
    avg_profiles = zeros(size(profiles));
    for i = 1:segment_count
        if segment_valid(i)
            avg_profiles(:, i) = profiles(:, i)./counts(:, i);
        end
    end
    
    % calculate valid profile for ER 
    ER_segment_valid = zeros(segment_count,1);
    for i = 1:segment_count
        if segment_valid(i)
            if sum(profiles_other(:, i)) == 0
                ER_segment_valid(i) = 1;
            end    
        end
    end
    
    
    % calculate derivatives for valid segments
    diff_avg_profiles = diff(avg_profiles, diff_step, 1);
    
    % run the first derivative maxima detection
    diff_max_coordinate = zeros(segment_count, 1);
    diff_max_area = zeros(segment_count, 1);
    diff_max_integ = zeros(segment_count, 1);
    segment_diff_valid = zeros(segment_count, 1);
    segment_diff_filter = zeros(segment_count, 1);
    for j=1:segment_count
        if segment_valid(j) 
            integr = 0; pixel = 0; value = 0; area = 0;
            max_integr = 0; max_pixel = 0; max_area = 0;
            for i = 1:length_nucl+length_cytopl-diff_step
                if diff_avg_profiles(i,j) > 0
                    integr = integr + diff_avg_profiles(i,j);
                    area = area + 1;
                    if diff_avg_profiles(i,j) > value
                        value = diff_avg_profiles(i,j);
                        pixel = i;
                    end
                    if integr > max_integr && pixel >= length_nucl - rim_search_nucl && pixel <= length_nucl + rim_search_cytopl
                        max_integr = integr;
                        max_pixel = pixel;
                        max_area = area;
                    end
                else
                    integr = 0; pixel = 0; value = 0; area = 0;
                end
            end
            if max_pixel ~= 0 
                segment_diff_valid(j) = 1;
                if max_area >= min_area && max_integr >= min_integr_int
                    segment_diff_filter(j) = 1;
                end
                diff_max_coordinate(j) = max_pixel;
                diff_max_area(j) = max_area;
                diff_max_integ(j) = max_integr;
            end
        end
    end
    
    % calculate the rim coordinate and intensity
    rim_intensity_search = zeros(segment_count, 1);
    rim_intensity_offset = zeros(segment_count, 1);
    rim_coordinate_search = zeros(segment_count, 1);
    rim_coordinate_offset = zeros(segment_count, 1);    
    cytoplasmic_inten = zeros(segment_count, 1);
    nucl_inten = zeros(segment_count, 1);
    nucl_rim_ratio = zeros(segment_count, 1);
    nucl_rim_offset_ratio = zeros(segment_count, 1);
    nucl_rim_cyto_ration = zeros(segment_count, 1);
    
    for j=1:segment_count
        if segment_diff_valid(j) && segment_valid(j)
            max_mean_of_3pix = 0; max_mean_of_3pix_center = 0;
            for i=diff_max_coordinate(j)+1:diff_max_coordinate(j)+3
                mean_of_3pix = mean(avg_profiles(i-1:i+1,j));
                if mean_of_3pix > max_mean_of_3pix
                    max_mean_of_3pix = mean_of_3pix;
                    max_mean_of_3pix_center = i;
                end                
            end
            rim_intensity_max(j) = max(avg_profiles(diff_max_coordinate(j):diff_max_coordinate(j)+4, j));
            rim_intensity_search(j) = max_mean_of_3pix;
            rim_intensity_offset(j) = mean(avg_profiles(diff_max_coordinate(j)+1:diff_max_coordinate(j)+3,j));
            rim_coordinate_search(j) = max_mean_of_3pix_center;
            rim_coordinate_offset(j) = diff_max_coordinate(j) + 2;
            cytoplasmic_inten(j) = mean(avg_profiles(rim_coordinate_search(j)+2:rim_coordinate_search(j)+5,j));
            nucl_inten(j) = mean(avg_profiles(rim_coordinate_search(j)-6:rim_coordinate_search(j)-2,j));
            nucl_rim_ratio(j) = nucl_inten(j)/rim_intensity_search(j);
            nucl_rim_offset_ratio(j) = nucl_inten(j)/rim_intensity_offset(j);
            nucl_rim_cyto_ration(j) = nucl_inten(j)/(rim_intensity_search(j) + cytoplasmic_inten(j));
        end
    end
    
    
    % compute mean of ER regione Extend
    ER_region_ext = zeros(((size(ER_segment_valid(ER_segment_valid == 1),1))), 2);
    i = 1;
    for j = 1:segment_count
        if ER_segment_valid(j) == 1
            ER_region_ext(i,:) = [j, mean(avg_profiles(rim_coordinate_search(j)+8:rim_coordinate_search(j)+25,j))];
            i = i + 1;
        end
    end
    
    % compute highest mean of ER regions Extend (average of num segments) and retriev the segment numbers 
    max_ER_region_ext = 0; mean_ER_region_ext = 0; num = round(size(ER_segment_valid(ER_segment_valid == 1),1)/4);
    
    if (size(ER_segment_valid(ER_segment_valid == 1),1)) > 0
        if num > 1
            num = num;
        else
            num = (size(ER_segment_valid(ER_segment_valid == 1),1));
        end
    
        for j = 1 : (ER_region_ext(end,1))
            mean_ER_region_ext = mean(ER_region_ext((1:num),2));
            if mean_ER_region_ext > max_ER_region_ext
               max_ER_region_ext = mean_ER_region_ext;
               ER_Segment_Ext = ER_region_ext(1:num,1);
            end
            ER_region_ext = circshift(ER_region_ext,1);
        end
    end
    
    % filter valid regions for analysis
    segment_final_filter = (nucl_rim_ratio < 0.6 & segment_diff_filter);   % original = segment_final_filter = (nucl_rim_ratio < 0.6 & segment_diff_filter);
    
    % do final segment rendering
    segment_plot = gray_to_rgb(image_gfp);    
    %segment_plot = imcomplement(segment_plot);
    avg_profiles_plot = gray_to_rgb(imresize(avg_profiles/max(avg_profiles(:)), 3, 'nearest'));
    diff_avg_profiles_plot = gray_to_rgb(imresize(diff_avg_profiles/max(diff_avg_profiles(:)), 3, 'nearest'));

    for j=1:segment_count
        if segment_valid(j) && segment_diff_valid(j)
            p1 = segment_starts(j,:); p2 = segment_ends(j,:);
            segment_plot = render_line(segment_plot, p1, p2, [1 1 1] );    %[1 1 1]                                            
            if segment_diff_filter(j)
                if segment_final_filter(j)
                    segment_color = [0 1 0];     %[0 1 0]
                    
                else
                    segment_color = [1 0 0];    %[1 0 0] 
                end   
            else
                segment_color = [0 0 1] ;   %[0 0 1] 
            end                
            p1 = origin(j,:); p2 = origin(j,:)+segment_vector_x(j,:)*step_length;
            segment_plot = render_line(segment_plot, p1, p2, segment_color);
            p1 = origin(j,:); p2 = origin(j,:)+segment_vector_y(j,:)*(length_nucl+length_cytopl);
            segment_plot = render_line(segment_plot, p1, p2, segment_color);
            avg_profiles_plot(3*diff_max_coordinate(j)-1, 3*j-1, :) = segment_color;
            avg_profiles_plot(3*rim_coordinate_search(j)-1, 3*j-1, :) = segment_color;
            
            if (size(ER_segment_valid(ER_segment_valid == 1),1)) > 0
                for i = 1 : length(ER_Segment_Ext)
                    if ER_Segment_Ext(i) == j
                        avg_profiles_plot(3*(rim_coordinate_search(j)+8)-1, 3*j-1, :) = [1 1 0];  % [1 1 0]
                        avg_profiles_plot(3*(rim_coordinate_search(j)+25)-1, 3*j-1, :) = [1 1 0];  % [1 1 0]
                    end
                end 
            else
                avg_profiles_plot(3*(rim_coordinate_search(j)+2), 3*j-1, :) = [1 1 0];  % [1 1 0]    
                avg_profiles_plot(3*(rim_coordinate_search(j)+5), 3*j-1, :) = [1 1 0];  % [1 1 0]   
            end
                
            diff_avg_profiles_plot(3*diff_max_coordinate(j)-1, 3*j-1, :) = segment_color;    
            diff_avg_profiles_plot(3*rim_coordinate_search(j)-1, 3*j-1, :) = segment_color;
            
            
        else
            p1 = segment_starts(j,:); p2 = segment_ends(j,:);
            segment_plot = render_line(segment_plot, p1, p2, [1 0 0]);  %[1 0 0]
            avg_profiles_plot(2, 3*j-1, :) = [1 0 0]; %[1 0 0]
            diff_avg_profiles_plot(2, 3*j-1, :) = [1 0 0];  %[1 0 0]
        end
        
        p1 = segment_starts(j,:); p2 = segment_ends(j,:);
        
        if (size(ER_segment_valid(ER_segment_valid == 1),1)) > 0
            for i = 1 : length(ER_Segment_Ext)
                if ER_Segment_Ext(i) == j
                   segment_plot = render_line(segment_plot, p1, p2, [1 1 0]);  %[1 1 0]
                end
            end    
        end
    end
    
    data.segment_count = segment_count;    
    data.segment_count_valid = sum(segment_valid);
    data.segment_count_diff_valid = sum(segment_diff_valid);
    data.segment_count_diff_filtered = sum(segment_diff_filter);
    data.segment_count_included = sum(segment_final_filter);
    data.rim_intensity = mean(rim_intensity_search(segment_final_filter));
    data.rim_intensity_offset = mean(rim_intensity_search(segment_final_filter));
    data.rim_intensity_max = mean(rim_intensity_max(segment_final_filter));
    if (size(ER_segment_valid(ER_segment_valid == 1),1)) > 0
        data.cytoplasmic_inten = max_ER_region_ext;
    else
        data.cytoplasmic_inten = mean(cytoplasmic_inten(segment_final_filter));
    end    
    data.nucl_inten = mean(nucl_inten(segment_final_filter));
    data.nucl_size = nuclear_size;
    
    previews.segment_plot = segment_plot;
    previews.avg_profiles = avg_profiles_plot;
    previews.diff_avg_profiles = diff_avg_profiles_plot;
end        
        

% 
    % 
    % 
%     %         subplot(2,3,2); imshow(diff_avg_profiles/max(max(diff_avg_profiles))); hold on;
%     %         coord = cat(2,(1:size(diff_max_coordinate))', diff_max_coordinate);
%     %         plot(coord(:,1), coord(:,2), 'w', 'LineWidth', 1); hold off;
%     % 
%     %         subplot(2,3,3); imshow(avg_profiles/max(max(avg_profiles))); hold on;
%     %         coord = cat(2,(1:size(diff_max_coordinate))', diff_max_coordinate);
%     %         plot(coord(:,1), coord(:,2), 'r', 'LineWidth', 1);
%     %         coord = cat(2,(1:size(rim_coordinate))', rim_coordinate);
%     %         plot(coord(:,1), coord(:,2), 'w', 'LineWidth', 1);
%     %         coord = cat(2,(1:size(rim_coordinate))', rim_coordinate+3);
%     %         plot(coord(:,1), coord(:,2), 'w', 'LineWidth', 1);
%     %         coord = cat(2,(1:size(rim_coordinate))', rim_coordinate+10);
%     %         plot(coord(:,1), coord(:,2), 'w', 'LineWidth', 1);
%     %         coord = cat(2,(1:size(rim_coordinate))', rim_coordinate-11);
%     %         plot(coord(:,1), coord(:,2), 'w', 'LineWidth', 1);
%     %         coord = cat(2,(1:size(rim_coordinate))', rim_coordinate-4);
%     %         plot(coord(:,1), coord(:,2), 'w', 'LineWidth', 1); hold off;
%     % 
%     %         subplot(2,3,5); plot(cat(2,rim_intensity, cytoplasmic_inten, nucl_inten));
%     %         subplot(2,3,6); plot(cat(2,nucl_rim_ratio,nucl_rim_cyto_ration));
% 
