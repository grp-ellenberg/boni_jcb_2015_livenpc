% expands an image to size_out placing it to the center and padding with zeros

function image_out = expand_image(image_in, size_out)

    size_in = [size(image_in, 1) size(image_in, 2)];
    
    paste_start = floor((size_out - size_in)/2) + 1;
    pase_end = paste_start + size_in - 1;

    image_out = zeros(cat(2, size_out, 3));
    image_out(paste_start(1):pase_end(1), paste_start(2):pase_end(2), :) = image_in;
    
end