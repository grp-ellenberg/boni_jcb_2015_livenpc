function [image_gfp, image_mcherry, image_region, image_region_all] = extract_images(time_point, region_index, hdf_data)

    % setting for mcherry and gfp channel index
    channel_mcherry = 1; channel_gfp = 2;

    % read overall image size and current region bounding box    
    image_size = size(hdf_data.channel(:, :, 1, time_point, channel_mcherry));
    bounding_box = [hdf_data.bounding_box.left(region_index) hdf_data.bounding_box.right(region_index) ...
                    hdf_data.bounding_box.top(region_index) hdf_data.bounding_box.bottom(region_index)];
    % expand bounding box size making sure it does not exceed image dimmensions
    bounding_box = expand_bounding_box(bounding_box, 50, image_size);

    % crop cell from the overall image using the expanded bounding box
    image_mcherry = hdf_data.channel(bounding_box(1):bounding_box(2), bounding_box(3):bounding_box(4), 1, time_point, channel_mcherry);
    image_gfp = hdf_data.channel(bounding_box(1):bounding_box(2), bounding_box(3):bounding_box(4), 1, time_point, channel_gfp);
    image_region = hdf_data.region(bounding_box(1):bounding_box(2), bounding_box(3):bounding_box(4), 1, time_point, channel_mcherry);
    image_region_all = hdf_data.region(bounding_box(1):bounding_box(2), bounding_box(3):bounding_box(4), 1, time_point, channel_mcherry);
    
    % erease regions corresponding to other cells
    region_label = hdf_data.region_id.obj_label_id(region_index);
    image_region = image_region == region_label;

end