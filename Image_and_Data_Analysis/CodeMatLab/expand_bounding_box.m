% expands bounding box by a defined number of pixels making sure that it
% does not exeed given image size

function bounding_box = expand_bounding_box(bounding_box, pixels, image_size)

    if bounding_box(1) > pixels
        bounding_box(1) = bounding_box(1) - pixels;
    else
        bounding_box(1) = 1;
    end
    if bounding_box(3) > pixels
        bounding_box(3) = bounding_box(3) - pixels;
    else
        bounding_box(3) = 1;
    end

    if bounding_box(2) < image_size(1) - pixels
        bounding_box(2) = bounding_box(2) + pixels;
    else
        bounding_box(2) = image_size(1);        
    end
    if bounding_box(4) < image_size(2) - pixels
        bounding_box(4) = bounding_box(4) + pixels;
    else
        bounding_box(4) = image_size(2);        
    end
    
end