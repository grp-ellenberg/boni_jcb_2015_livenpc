function experiments_tree = generate_data_tree(input_folder_name, output_folder_name)

    % make sure folders and by back slash
    if ~strcmp(input_folder_name(end), '\')
        input_folder_name = input_folder_name + '\';
    end
    if ~strcmp(output_folder_name(end), '\')
        output_folder_name = output_folder_name + '\';
    end
    
    % get folder names which correspond to experiment names
    experiments = dir(input_folder_name);
    experiments = experiments(cellfun(@(x) x==true, {experiments(:).isdir}));
    experiments = experiments(cellfun(@(x) ~strcmp(x, '.') && ~strcmp(x, '..'), {experiments(:).name}));
    experiments = {experiments(:).name};
    
    % iterate for each experiment
    for i_experiment = 1:length(experiments)
        
        % generate full folder names to read/write data
        experiments_tree(i_experiment).name = experiments{i_experiment}; %#ok
        experiments_tree(i_experiment).input_folder = [input_folder_name experiments{i_experiment} '\']; %#ok
        experiments_tree(i_experiment).output_folder = [output_folder_name experiments{i_experiment} '\']; %#ok

        % get the hdf5 file names that correspond to positon names,
        position_file_names = dir([input_folder_name experiments{i_experiment} '\hdf5\*.hdf5']); %% change to ch5 from hdf5
        position_file_names = {position_file_names(:).name};
        
        % iterate through positons, assign name, generate hdf5 file path, output folder name
        for i_position = 1:length(position_file_names)
            
            [~, positoin_name, ~] = fileparts(position_file_names{i_position});
            experiments_tree(i_experiment).positions(i_position).name = positoin_name; %#ok
            experiments_tree(i_experiment).positions(i_position).hdf_file_name = ...
                [input_folder_name experiments_tree(i_experiment).name '\hdf5\' position_file_names{i_position}]; %#ok
            experiments_tree(i_experiment).positions(i_position).output_folder = ...
                [experiments_tree(i_experiment).output_folder positoin_name '\']; %#ok
        end
    end    
    
end

