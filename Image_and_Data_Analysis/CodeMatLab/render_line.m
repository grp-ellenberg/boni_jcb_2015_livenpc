% renders a line on an image setting its pixels to  given value

function img = render_line(img, line_start, line_end, value)

    line_length = sqrt(sum((line_start - line_end).^2));
    step_count = floor(line_length * 5);
    step_size = (line_end - line_start)/step_count;
    line_coord_y = line_start(1) + (1:step_count) * step_size(1);
    line_coord_x = line_start(2) + (1:step_count) * step_size(2);    
    
    line_ind = sub2ind(size(img), round(line_coord_y), round(line_coord_x), zeros(1, step_count) + 1);
    img(line_ind) = value(1);

    line_ind = sub2ind(size(img), round(line_coord_y), round(line_coord_x), zeros(1, step_count) + 2);
    img(line_ind) = value(2);

    line_ind = sub2ind(size(img), round(line_coord_y), round(line_coord_x), zeros(1, step_count) + 3);
    img(line_ind) = value(3);

end