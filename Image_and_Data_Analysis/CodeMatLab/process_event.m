function [time_points_results, event_preview] = process_event(event_data)
    
    % iterate through timepoints, quantify tim and generate preview image
    event_preview = [];
    for i_time_point = 1:length(event_data.time_points)
        %disp(i_time_point)
        % retriev images from event_data structure
        image_gfp = event_data.time_points(i_time_point).gfp;
        image_mcherry = event_data.time_points(i_time_point).mcherry;
        image_region = event_data.time_points(i_time_point).region;
        image_region_all = event_data.time_points(i_time_point).region_all;
        
        % run rim analtsis on current event and time point, append results to time_points
        [time_points_results(i_time_point), time_point_previews(i_time_point)] = quantify_rim_image(image_gfp, image_region, image_region_all); %#ok
    end

    % first calculate maximum image size over all timepoints in current event
    max_size = [0 0];
    for i_time_point = 1:length(event_data.time_points)
        max_size = max(max_size, size(event_data.time_points(i_time_point).gfp));
        max_size = max(max_size, size(event_data.time_points(i_time_point).mcherry));
        max_size = max(max_size, size(event_data.time_points(i_time_point).region));
        max_size = max(max_size, size(time_point_previews(i_time_point).avg_profiles(:,:,1)));
        max_size = max(max_size, size(time_point_previews(i_time_point).segment_plot(:,:,1)));   
        max_size = max(max_size, size(time_point_previews(i_time_point).diff_avg_profiles(:,:,1)));
    end
    
    % concatenate preview image to montage
    for i_time_point = 1:length(event_data.time_points)
        image_gfp = event_data.time_points(i_time_point).gfp;
        image_mcherry = event_data.time_points(i_time_point).mcherry;
        image_region = event_data.time_points(i_time_point).region;        
        
        preview_gfp = expand_image(gray_to_rgb(image_gfp), max_size);
        preview_mcherry = expand_image(gray_to_rgb(image_mcherry), max_size);
        preview_region = expand_image(gray_to_rgb(uint8(image_region)*255), max_size);
        preview_segment_plot = expand_image(time_point_previews(i_time_point).segment_plot, max_size);
        preview_avg_profiles = expand_image(time_point_previews(i_time_point).avg_profiles, max_size);
        preview_diff_avg_profiles = expand_image(time_point_previews(i_time_point).diff_avg_profiles, max_size);

        event_preview = cat(2, event_preview, cat(1, preview_mcherry, preview_region, preview_gfp, ...
                                                  preview_segment_plot, preview_avg_profiles, preview_diff_avg_profiles));                
    end    
    
end