% exports structure containing all experimental results into a text file
function write_experiment_data(positions_results, experimnet_name, output_file_name)

    file_out = fopen(output_file_name, 'w');

    % useful constant string
    char_tab = sprintf('\t');
    char_newline = sprintf('\r\n');
    
    % generate header line and output it
    line_header = ['experiment_name' char_tab 'position_name' char_tab 'event_name' char_tab 'time_point'];
    time_point_fields = fields(positions_results(1).events_results(1).time_points(1));
    for i_time_point_field = 1:length(time_point_fields)
        line_header = [line_header char_tab time_point_fields{i_time_point_field}]; %#ok
    end
    fwrite(file_out, line_header);
    
    % iterate over all positions and events in current experiment results
    for i_position = 1:length(positions_results)
        for i_event = 1:length(positions_results(i_position).events_results)            
            for i_time_point = 1:length(positions_results(i_position).events_results(i_event).time_points)
                fwrite(file_out, char_newline);

                % initialize line to be printed in output file
                line_data = [experimnet_name char_tab ...
                             positions_results(i_position).name char_tab ...
                             positions_results(i_position).events_results(i_event).name char_tab ...
                             num2str(i_time_point)];

                % get field names and iterate over all of them printing theresults
                for i_time_point_field = 1:length(time_point_fields)
                    field_value = positions_results(i_position).events_results(i_event).time_points(i_time_point).(time_point_fields{i_time_point_field});
                    line_data = [line_data char_tab num2str(field_value)]; %#ok
                end

                fwrite(file_out, line_data);
            end            
        end
    end
    
    fclose(file_out);
end