% exports structure containing all experimental results into a text file
function write_experiment_positions(positions, experimnet_name, output_file_name)

    file_out = fopen(output_file_name, 'w');

    % useful constant string
    char_tab = sprintf('\t');
    char_newline = sprintf('\r\n');
    
    % generate header line and output it
    line_header = ['experiment_name' char_tab 'position_name'];
    fwrite(file_out, line_header);
    
    % iterate over all positions and events in current experiment results
    for i_position = 1:length(positions)
        fwrite(file_out, char_newline);
            
        % initialize line to be printed in output file
        line_data = [experimnet_name char_tab positions(i_position).name];
            
        fwrite(file_out, line_data);
    end
    
    fclose(file_out);
end