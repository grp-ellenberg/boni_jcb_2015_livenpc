clear all; clc;

% set input and output folder containing labek subfolder
input_folder_name = '..\CellCognition\AnalysisCC\';
output_folder_name = '..\OutputMatLab\';
%input_folder_name = 'C:\Users\Ellenberg Lab\Desktop\Screening\BugTrackingIn\';
%output_folder_name = 'C:\Users\Ellenberg Lab\Desktop\Screening\BugTrackingOut\';

% iterates through folder and file structure and returs tree of experiments and positions
experiments = generate_data_tree(input_folder_name, output_folder_name);

% iterates through experiments in experiments tree
for i_experiment = 1:length(experiments)
    
    % allocate memory for results of all positions in current experiment
    position_count = length(experiments(i_experiment).positions);

    % counter of positions with events
    i_position_with_events = 0;
    
    % iterates through positions in current experiment
    for i_postion = 1:position_count
        
        % printing information about current positon, creating output folder
        disp(['Experiment name: ' experiments(i_experiment).name])
        disp(['Experiment input folder: ' experiments(i_experiment).input_folder])
        disp(['Experiment output folder: ' experiments(i_experiment).output_folder])
        disp(['Position name: ' experiments(i_experiment).positions(i_postion).name])
        disp(['Position HDF5 file name: ' experiments(i_experiment).positions(i_postion).hdf_file_name])
        disp(['Position output folder: ' experiments(i_experiment).positions(i_postion).output_folder])
        mkdir(experiments(i_experiment).positions(i_postion).output_folder);                
        
        % read data from cell cognition HDF5 file and reformat to more convinient data structure
        disp('Reading and reformating HDF5 file...');
        position_hdf_data = read_hdf_data(experiments(i_experiment).positions(i_postion).hdf_file_name, ...
                                          experiments(i_experiment).name,...
                                          experiments(i_experiment).positions(i_postion).name);        
        
        % if there are no events for this data point skip analysis
        if isempty(position_hdf_data.event)
            continue
        end
        
        events_input_data = format_hdf_data(position_hdf_data);        
        clear position_hdf_data;        

        % generate variables needed inside the parfor loop bellow
        position_output_folder = experiments(i_experiment).positions(i_postion).output_folder;
        event_count = length(events_input_data);
        clear events_results;
        events_results = struct('time_points', cell(event_count,1), 'name', cell(event_count,1));
        
        % parfor loop executing data nalysis for each event (cell) in the postion
        fprintf('Processing event:\n\n');
        parfor i_event = 1:event_count
%         for i_event = 17 % 1:event_count    
            %disp(i_event)
            % process event and append results to events_results
            fprintf(['\b' num2str(i_event) '> \n'])
            [events_results(i_event).time_points, event_preview] =  process_event(events_input_data(i_event));
            event_name = ['event' num2str(i_event)];
            events_results(i_event).name = event_name;
            
            % write event preview image
            imwrite(event_preview, [position_output_folder event_name '_preview.jpg'], 'Quality' , 100)            
            fprintf(['\b<' num2str(i_event) ' \n'])            
        end
        
        % save events results for current position in a positions_results
        i_position_with_events = i_position_with_events + 1;
        positions_events_results(i_position_with_events).events_results = events_results; %#ok
        positions_events_results(i_position_with_events).name = experiments(i_experiment).positions(i_postion).name; %#ok
        fprintf('Done!\n\n')
    end
    
    % export save data for all positions and events in current experiment into csv file
    write_experiment_data(positions_events_results, experiments(i_experiment).name, ...
    	[output_folder_name experiments(i_experiment).name '_events.txt']);
    write_experiment_positions(experiments(i_experiment).positions, experiments(i_experiment).name, ...
    	[output_folder_name experiments(i_experiment).name '_positions.txt'])
    
    % export data for all positions and events in current experiment into mat file    
    save([output_folder_name experiments(i_experiment).name '_events.mat'], 'positions_events_results');
    positions_list = experiments(i_experiment).positions;
    save([output_folder_name experiments(i_experiment).name '_positions.mat'], 'positions_list')
    clear positions_events_results;
end
