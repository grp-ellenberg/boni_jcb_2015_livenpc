function hdf_data_formated = format_hdf_data(hdf_data)

    % event counted needed because every line does not correspond to one event
    i_event = 0;

    % iterate through lines in events table
    for i_event_line = 1:length(hdf_data.event.obj_id)

        % read event id
        event_id = hdf_data.event.obj_id(i_event_line);
        
        % starting a new event on a first line or when event id changes
        if (i_event_line == 1 || previous_event_id ~= event_id)
            i_event = i_event + 1;
            
            % initialize time point counter for new event and asign timepoint data
            % use idx1 as index of time point segmentation region
            i_time_point = 1;
            region_index = hdf_data.event.idx1(i_event_line) + 1;            
            [hdf_data_formated(i_event).time_points(i_time_point).gfp, ...
             hdf_data_formated(i_event).time_points(i_time_point).mcherry, ...
             hdf_data_formated(i_event).time_points(i_time_point).region...
             hdf_data_formated(i_event).time_points(i_time_point).region_all] = extract_images(i_time_point, region_index, hdf_data); %#ok
        end

        % incremetn time point counter and asign timepoint data
        % use idx2 as index of time point segmentation region        
        i_time_point = i_time_point + 1;        
        region_index = hdf_data.event.idx2(i_event_line) + 1;        
        [hdf_data_formated(i_event).time_points(i_time_point).gfp, ...
         hdf_data_formated(i_event).time_points(i_time_point).mcherry, ...
         hdf_data_formated(i_event).time_points(i_time_point).region...
         hdf_data_formated(i_event).time_points(i_time_point).region_all] = extract_images(i_time_point, region_index, hdf_data); %#ok
        
        % save previous event_id to check if next line contains identical event
        previous_event_id = event_id;
    end
    
end