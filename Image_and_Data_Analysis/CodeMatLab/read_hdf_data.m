function hdf_data = read_hdf_data(hdf_file_name, plate, position)

    % open hdf5 file with lowe level reating function
    hdf_file = H5F.open(hdf_file_name, 'H5F_ACC_RDONLY', 'H5P_DEFAULT');
    
    % set dataset path root according to cell cognition internal hdf5 data structure
    dataset_path = ['/sample/0/plate/' plate '/experiment/0/position/' position];

    % read valid datasets including events, images, bounding boxes and region ids
    dset = H5D.open(hdf_file, [dataset_path '/object/event']);
    hdf_data.event = H5D.read(dset);
    H5D.close(dset);
    dset = H5D.open(hdf_file, [dataset_path '/image/channel']);
    hdf_data.channel = H5D.read(dset);
    H5D.close(dset);
    dset = H5D.open(hdf_file, [dataset_path '/image/region']);
    hdf_data.region = H5D.read(dset);
    H5D.close(dset);
    dset = H5D.open(hdf_file, [dataset_path '/feature/primary__primary/bounding_box']);
    hdf_data.bounding_box = H5D.read(dset);
    H5D.close(dset);
    dset = H5D.open(hdf_file, [dataset_path '/object/primary__primary']);
    hdf_data.region_id = H5D.read(dset);
    H5D.close(dset);

    % hdf5 file cosing
    H5F.close(hdf_file);
    
end