%%
% LowRes20x_CecogAnalysis.m
% 
% Performs watershed segmentation of forground signal (cytoplasmic +
% nucleus) using nuclei coordinates as seed. Returns the single cell total
% fluorescence trajectories. 
% step 4 for computing single cell total fluorescent signal,
% Previous steps are:	
%   1) Split the multi tif lsm files into single tif files to be read in CellCognition. For this you can use the FiJi macro Micronaut_Split_Channels_.py.
%	2) Classify and track the cells according to the H2B signal. Example  Classifier is stored in ./Cecog/Classifier.  Use the naming scheme Micronaut in 	CellCognition.
%	3) Compute the foreground signal in Fiji using intensityAllCells.py. 
% requires Bioformats tools for MATLAB see http://downloads.openmicroscopy.org/bio-formats/5.1.0/
%
% INPUT: 
%   -cecogDir: contains Analysed Hdf5 file with coordinates and classes
%   -imageDir: contains images where background have been set to 0. These ahave the ending *.lsm_mask.tif
%   -wellPosCell: A cell structure with the number of well that will be
%   merged together, e.g. {[1:2], [3:4]}
%   -name of output files. One per wellPos, e.g. {'condition1.txt',
%   'condition2.txt'}
% OUTPUT:
%   -outputfiles that contain intensity for each cell.
%
% The nuclei signal is obtained from cecog results file
% Remove cells that have divided or where the intensity is below 10 at the
% start.
% 
% Author: Antonio Politi, EMBL
% Code used for Boni et al. (2015) Journal of Cell Biology Fig. S3D,E
%

function LowRes20x_CecogAnalysis(cecogDir, imageDir, wellPosCell, outFileNames)
try 
    bfGetReader
catch
    errordlg('You need bioformat tool for matlab. Please download the tools from http://downloads.openmicroscopy.org/bio-formats/5.1.0/')
    return
end
if nargin < 1
   cecogDir = fullfile(fileparts(mfilename('fullpath')), 'Cecog');
end
if nargin < 2
   imageDir = fullfile(fileparts(mfilename('fullpath')), 'images');
end
if nargin < 3
    wellPosCell = {[1]};
end
if nargin < 4
    outFileNames = {'demo.txt'};
end

runAnalysis(cecogDir, imageDir, wellPosCell, outFileNames);


end

function runAnalysis(cecogDir, imageDir, wellPosCell, outFileNames)
%main pipeline
for iCell = 1:length(wellPosCell)
    wellPos = wellPosCell{iCell};
    TotalAllWells = [];
    NucAllWells = [];
    ERAllWells = [];
    AreasAll = [];
    for i = wellPos
        if i< 10
            wellnr = ['0000' num2str(i)];
        else
            wellnr = ['000' num2str(i)];
        end
        try
            [TotalFl, NucFl, ERFl, Areas] = singleCellDegradation(cecogDir, imageDir, wellnr);
            TotalAllWells = [TotalAllWells TotalFl];
            NucAllWells = [NucAllWells  NucFl];
            ERAllWells = [ERAllWells  ERFl];
            AreasAll = [AreasAll Areas];
        end
            
    end
    %%
    bg = 1.95;
    tPoints = size(TotalAllWells,1);
    %mean of signal for all single cells
    mTotal = mean((TotalAllWells-bg)./repmat(TotalAllWells(1,:)-bg,tPoints,1),2);
    %mean of signal for all single cells
    stdTotal = std((TotalAllWells-bg)./repmat(TotalAllWells(1,:)-bg,tPoints,1),1,2);
    %Total signal where change in area (due to segmentation has been take
    %into account
    TotAll = (TotalAllWells-bg).*AreasAll./repmat(AreasAll(1,:),tPoints,1)
    mTotal_Area = mean(TotAll./repmat(TotAll(1,:),tPoints,1),2);
    stdTotal_Area =  std(TotAll./repmat(TotAll(1,:),tPoints,1),1,2);
    %ER signal (Total -Nuc)
    mER = mean((ERAllWells-bg)./repmat(ERAllWells(1,:)-bg,tPoints,1),2);
    stdER =  std((ERAllWells-bg)./repmat(ERAllWells(1,:)-bg,tPoints,1),1,2);
    %Nuc signal 
    mNuc = mean((NucAllWells-bg)./repmat(NucAllWells(1,:)-bg,tPoints,1),2);
    stdNuc =  std((NucAllWells-bg)./repmat(NucAllWells(1,:)-bg,tPoints,1),1,2);
    
    figure(1)
    clf
    errorbar([0:size(mTotal,1)-1]*15, mTotal, stdTotal);
    meanOfCells = [[0:size(mTotal,1)-1]'*15 mTotal stdTotal mTotal_Area stdTotal_Area mTotal stdTotal mER stdER mNuc stdNuc ];
    save(fullfile(mainDir, ['mean_' outFileNames{iCell}]), 'meanOfCells', '-ascii');
    save(fullfile(mainDir,['singleTotal_' outFileNames{iCell}]), 'TotalAllWells', '-ascii');
end

end

function [TotalFl, NucFl, ERFl, Areas] = singleCellDegradation(cecogDir, imageDir, wellnr)
%read regions from hdf5 file. These are the segmented H2B signal

h5file = fullfile(cecogDir, 'Analysis', 'hdf5', [wellnr '_01.ch5']);
try
imgNucAll = h5read(h5file,...
    ['/sample/0/plate/images/experiment/' wellnr '/position/1/image/region']);
catch
    errordlg('Cecog hdf5 file has not been found. Process data first with cellCognition and create a hdf5 results file');
end
%These is the forground signal for the GFP, obtained from FiJi macro
maskFileNames = dir(fullfile(imageDir, '*.lsm_mask.tif'));
if isempty(maskFileNames)
    errordlg(['No foreground mask with name filename.lsm_mask.tif found  ' imageDir ...
        '\n. Process data with intensityAllCells.py in Fiji'])
    return
end
tok = regexp(maskFileNames(1).name,'(\w+_+W\d+)\d(_+\w+)', 'tokens');
if isempty(tok)
    tok = regexp(maskFileNames(1).name,'(W\d+)\d(_+\w+)', 'tokens');
end
prefix = char(tok{1}(1));
if str2num(wellnr) > 9
    reader = bfGetReader(fullfile(imageDir, [prefix(1:end-1) num2str(str2num(wellnr))  char(tok{1}(2)) '.lsm_mask.tif']));
else
    reader = bfGetReader(fullfile(imageDir, [prefix num2str(str2num(wellnr))  char(tok{1}(2)) '.lsm_mask.tif']));
end
%%
%reads which cells have not went through mitosis (look for non-occurance of
%mitotic events)
cellOI = readEventsHDF5(cecogDir, h5file);

nrTimePts = size(cellOI,1);
cellLab = cell(nrTimePts,1);
imgs = cell(nrTimePts ,1);

TotalFl = double(0*cellOI);
ERFl = double(0*cellOI);
NucFl = double(0*cellOI);
Areas = double(0*cellOI);
for tpoint = 1:nrTimePts
    display(['Process time point ' num2str(tpoint) '/' num2str(nrTimePts) ])
    imgNuc = imgNucAll(:,:,1,tpoint)';
    img = bfGetPlane(reader, tpoint);
    [TotalFl(tpoint,:), ERFl(tpoint,:), NucFl(tpoint,:),  Areas(tpoint,:), cellLab{tpoint}, imgs{tpoint}] = processTpt(tpoint, imgNucAll, reader, cellOI);
end
%%
%find cells that have intensity larger than 10 (close to background of
%non-expressing cells).
idxGC = find((TotalFl(1,:)>10).*(sum(TotalFl == 0,1)==0));

for tpoint = 1:nrTimePts
    display(['Create video for time point ' num2str(tpoint) '/' num2str(nrTimePts) ])
    goodCells = cellOI(tpoint,idxGC);
    CC = bwconncomp(cellLab{tpoint});
    mapReI = regionprops(CC, cellLab{tpoint}, 'MaxIntensity');
    mapReI = cat(1,mapReI.MaxIntensity);
    for i=1:CC.NumObjects
        if sum(goodCells == mapReI(i)) < 1
            cellLab{tpoint}(CC.PixelIdxList{i}) = 0;
        else
            cellLab{tpoint}(CC.PixelIdxList{i}) = 1;
        end
    end
%%
    figure(1)
    clf
    imshow(imgs{tpoint});

    hold on;
    h2 = imshow(label2rgb(bwlabel(cellLab{tpoint})));
    set(h2, 'AlphaData', (cellLab{tpoint} == 0)*0 + (cellLab{tpoint} > 0)*0.2);
    hold off;
    m(tpoint) = getframe(1);
end
%%
try
    movie2avi(m, fullfile(mainDir, [char(wellnr) '.avi']), 'fps', 2);
end
TotalFl = TotalFl(:,idxGC);
NucFl = NucFl(:,idxGC);
ERFl = ERFl(:,idxGC);
Areas = Areas(:,idxGC);
end

function [TotalFl, ERFl, NucFl, Areas, updateLabels, img] = processTpt(tpoint, imgNucAll, reader, cellOI)
TotalFl = double(0*cellOI(tpoint,:));
ERFl = double(0*cellOI(tpoint,:));
NucFl = double(0*cellOI(tpoint,:));
Areas = double(0*cellOI(tpoint,:));
AreasNuc = double(0*cellOI(tpoint,:));
imgNuc = imgNucAll(:,:,1,tpoint)';
img = bfGetPlane(reader, tpoint);

%%
%create bg lines using distance transform
bw = im2bw(img, 0);
bw = imclose(bw, ones(3,3));
D = bwdist(bw);
DL = watershed(D);
bgm = DL == 0;
bwNuc = im2bw(double(imgNuc)/double(max(max(imgNuc))), 0);
imgCom = imcomplement(img);
I_mod = imimposemin(imgCom, bgm | bwNuc);
L = watershed(I_mod);
segCells = imclearborder(double(L>0).*double(bw));
labSegCells = bwlabel(segCells);
updatedNuc = (double(segCells).*double(imgNuc));
%%
updateLabels = labSegCells;

for i = 1:max(max(labSegCells))
    
    idx = find(labSegCells == i);
    val = max(max(double(imgNuc).*double(labSegCells==i)));
    
    if val > 0
        if any(cellOI(tpoint, :) == val)
            updateLabels(idx) = val;
        else
            updatedNuc(find(imgNuc == val)) = 0;
            updateLabels(idx) = 0;
        end
    else
        updateLabels(idx) = 0;
    end
end

%%
CC = bwconncomp(updateLabels);
CCNuc = bwconncomp(updatedNuc);
mapReI = regionprops(CC, updateLabels, 'MaxIntensity');
mapReI = cat(1,mapReI.MaxIntensity);
area =  regionprops(CC, updateLabels, 'Area');
areaNuc = regionprops(CCNuc, updateLabels, 'Area');
area = cat(1,area.Area);
areaNuc = cat(1, areaNuc.Area);
for i=1:CC.NumObjects
    idxTrackCell = find(cellOI(tpoint,:) == mapReI(i));
    Areas(idxTrackCell) = area(i);
    AreasNuc(idxTrackCell) = areaNuc(i);
    TotalFl(idxTrackCell) = double(mean(img(CC.PixelIdxList{i})));
    idxER = find(~ismember(CC.PixelIdxList{i}, CCNuc.PixelIdxList{i}));
    ERFl(idxTrackCell) = mean(img(CC.PixelIdxList{i}(idxER)));
    NucFl(idxTrackCell) = mean(img(CCNuc.PixelIdxList{i}));
end
end