function readFileHDF5(mainDir, h5file)
if nargin < 1  
    mainDir = 'C:\Users\toni\Science\AndreaNucTranslocation\ExperimentalData\WT_DeltaLBD_CHX\CHXExp_LowMag\CHX_WT\CHX50_130905\CHX50WO\Cecog';
end
if nargin < 2
    h5file = [mainDir '\Analysis\hdf5\00001_01.ch5']
end

if isempty(strfind(h5file, '_all_positions.ch5'))
    [path, fname] = fileparts(h5file);
    wellnr = strsplit(fname, '_');
    wellnr = wellnr(1);
else
    wellnr = '';
end
cellOI = processPosition(h5file, wellnr)
end
function cellOI = processPosition(h5file, wellnr)
for iWell=1:length(wellnr)
     ev = h5read(h5file, ['/sample/0/plate/images/experiment/' char(wellnr(iWell)) '/position/1/object/event']);
     obj = h5read(h5file, ['/sample/0/plate/images/experiment/' char(wellnr(iWell)) '/position/1/object/primary__primary']);
     for i=1:length(ev.idx1)/11
        cellOI(:,i) = [obj.obj_label_id(ev.idx1(1+(i-1)*11:11*i)+1); obj.obj_label_id(ev.idx2(11*i)+1)];
     end
end
end
% if strfind(h5file, '_all_positions.ch5')
%     
% for i=1:22
%     try
%         if i< 10
%             wellnr = ['0000' num2str(i)];
%         else
%             wellnr = ['000' num2str(i)];
%         end
%         ev = h5read(h5File, ['/sample/0/plate/images/experiment/' wellnr '/position/1/object/event']);
%         obj = h5read(h5File, ['/sample/0/plate/images/experiment/' wellnr '/position/1/object/primary__primary']);
%         %the counter is base 0 so in matlab you need to add 1
%         clear('cellOI')
%         for i=1:length(ev.idx1)/11
%             cellOI(:,i) = [obj.obj_label_id(ev.idx1(1+(i-1)*11:11*i)+1); obj.obj_label_id(ev.idx2(11*i)+1)];
%         end
%         a = 1;
%         dlmwrite(fullfile(outDir,[wellnr '_01.txt']), cellOI, 'delimiter','\t','precision',3)
%     catch
%         display(['No ' wellnr])
%     end
%         
% end
%obj.obj_label_id(ev.idx1(1:10)+1) 