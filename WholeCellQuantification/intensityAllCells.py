'''
intensityAllCells.py
	ImageJ macro to segment set background of image to 0 fo time lapse image. Operation is only performed on 1st channel, 2nd channel is used to identify nuclear signal
	-INPUT: Directory containing *.lsm files. Images have 2 channels (Target-INM-LBR-GFP and H2B-mCherry)
	-OUTPUT: multipage tif of first channel where background has been set to 0 (filename_mask.tif), summary statistics for the mean intensities for all cells. 
	
	Macro will search for files ending with .lsm in a directory
	For each file it will segment 2nd channel (H2B-mCherry = nuclei) 
	Segment 1st channel (GFP = Target-INM). Threshold is computed on the image - nuclear signal.
	This allows to compute location of backgroun
	Author: Antonio Politi, EMBL, 2015
	Code used in Boni et al. (2015) Journal of Cell Biology
'''

from ij import IJ
from ij.plugin import Duplicator, ImageCalculator
from ij.measure import Measurements
from util.opencsv import CSVWriter
from java.io import FileWriter
from java.lang.reflect import Array
from java.lang import String, Class
from jarray import array as jarr
from loci.plugins import BF
from ij.io import DirectoryChooser
import os
from glob import glob

def runOnFile(currDir, filename):
	#open the file
	imp = BF.openImagePlus(filename);
	imp = imp[0]
	#set scale to 0
	IJ.run(imp, "Set Scale...", "distance=0 known=0 pixel=1 unit=pixel")
	copy = Duplicator().run(imp, 1, 1, 1, 1, 1, imp.getNFrames())
	
	#segment nuclei (2nd channel) and create a mask of it
	nucleiMask  = createNucleiMask(imp)
	#segment GFP (TARGET-INM, 1st channel and create a mask)
	signalMask, areaMask, signalMaskER, areaMaskER = createSignalMask(imp, nucleiMask)

	#create image of total cell (wit background set to 0)
	signalImg = ImageCalculator().run("create multiply stack", signalMask, copy)
	#create image of ER (without background and nuclei set to 0)
	signalER = ImageCalculator().run("create multiply stack", signalMaskER, copy)
	signalImg.setTitle("signalTotal")
	signalER.setTitle("signalER")
	
	#compute statistics
	signalInt = []
	signalIntER = []
	signalArea = []
	signalAreaER = []
	my_file = open(filename + '.txt','w')
	my_file.write('%meansig/meansig(0) areasig/areasig(0) meansig areasig meansigER/meansigER(0) areasigER/areasigER(0) meansigER areasigER\n')
	for i in range(0,signalImg.getNFrames()):
		signalImg.setT(i+1)
		signalER.setT(i+1)
		
		statsSig = signalImg.getStatistics(Measurements.MEAN + Measurements.AREA)
		statsSigER = signalER.getStatistics(Measurements.MEAN + Measurements.AREA)
		
		#statsSig.area this is the total signal 
		signalInt.append(statsSig.mean*statsSig.area/areaMask[i])
		signalIntER.append(statsSigER.mean*statsSigER.area/areaMaskER[i])
		
		tmp = "%.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f\n" %(signalInt[i]/signalInt[0], areaMask[i]/areaMask[0], signalInt[i], areaMask[i], signalIntER[i]/signalIntER[0], areaMaskER[i]/areaMaskER[0], signalIntER[i], areaMaskER[i])
		my_file.write(tmp)
		
		#signalInt.append(str(meanSig-meanBg)*area_signal[i]/area_signal[0])
	my_file.close()
	IJ.run("Close All")
	return signalImg, [s/signalInt[0] for s in signalInt], signalInt, areaMask, signalER, [s/signalIntER[0] for s in signalIntER], signalIntER, areaMaskER


def createNucleiMask(imp):
	'''
	segment 2nd channel (H2B-mCherry) and returns a mask of it
	'''
	nucleiMask = Duplicator().run(imp, 2, 2, 1, 1, 1, imp.getNFrames())
	IJ.run(nucleiMask, "Median...", "radius=2 stack");
	IJ.run(nucleiMask, "Gaussian Blur...", "radius=2 stack");
	IJ.setAutoThreshold(nucleiMask, "Huang dark")
	IJ.run(nucleiMask, "Convert to Mask", "method=Huang stack background=Dark calculate black")
	IJ.run(nucleiMask, "Close-", "stack");
	IJ.run(nucleiMask, "Fill Holes", "stack");
	IJ.run(nucleiMask, "Analyze Particles...", "size=500-Infinity circularity=0.00-1.00 show=Masks in_situ stack");
	IJ.run(nucleiMask, "Dilate", "stack")
	return nucleiMask

def createSignalMask(imp, nucleiMask):
	'''
	segment 1st channel (Target-INM-LBR-GFP). First find threshold only on the cytoplasm - nuclei signal. 
	Then add the two. Returns an image that only highlights the cell region, the total area, ER-region, total ER region
	'''
	signalMask = Duplicator().run(imp, 1, 1, 1, 1, 1, imp.getNFrames())
	IJ.run(signalMask, "Median...", "radius=2 stack");
	IJ.run(signalMask, "Gaussian Blur...", "sigma=5 stack");
	nucMask = Duplicator().run(nucleiMask, 1,1, 1, 1, 1, nucleiMask.getNFrames())
	
	IJ.run(nucMask,"Invert", "stack");
	#multiply with inverted mask of nuclei. This just shows the cytoplasmic signal and background.
	#This avoids the darker area inside the nuclei and the bright area 
	#the coefficient is to X1
	IJ.run(nucMask, "Multiply...", "value=0.0039215686 stack");
	signalMaskER = ImageCalculator().run("create multiply stack", signalMask, nucMask)
	IJ.run(signalMaskER, "Convert to Mask", "method=Li stack background=Dark calculate black")
	
	signalMask =  Duplicator().run(signalMaskER, 1, 1, 1, 1, 1, signalMaskER.getNFrames())
	IJ.run(signalMask, "Convert to Mask", "method=Li stack background=Dark calculate black")
	
	#ER + nuclei mask
	ImageCalculator().run("add stack", signalMask, nucleiMask)
	IJ.run(signalMask, "Analyze Particles...", "size=500-Infinity circularity=0.00-1.00 show=Masks in_situ stack");
	area = []
	#normalize
	IJ.run(signalMask, "Multiply...", "value=0.0039215686 stack");
	#compute total fluorescence of all cells in ER and nucleus
	for i in range(0,signalMask.getNFrames()):
		signalMask.setT(i+1)
		stats = signalMask.getStatistics(Measurements.AREA + Measurements.MEAN)
		area.append(stats.mean*stats.area)
	#compute total fluorescence in ER on;y
	IJ.run(signalMaskER, "Analyze Particles...", "size=500-Infinity circularity=0.00-1.00 show=Masks in_situ stack");
	areaER = []
	IJ.run(signalMaskER, "Multiply...", "value=0.0039215686 stack");
	for i in range(0,signalMaskER.getNFrames()):
		signalMaskER.setT(i+1)
		stats = signalMaskER.getStatistics(Measurements.AREA + Measurements.MEAN)
		areaER.append(stats.mean*stats.area)
	
	return signalMask, area, signalMaskER, areaER

		
def executeOnDir(root, DT):
	#run recursively on a directory and find all files ending with .lsm
	ifiles = glob(root+"/*.lsm")
	if len(ifiles) > 0:
		IJ.log("intensityAllCells.py: %d image(s) found in %s" %(len(ifiles), root))
		currDir = root
		summary_file = open(currDir + '/summary.txt', 'w')
		summary_file.write('%time meansig/meansig(0) meansig areasig meansigER/meansigER(0) meansigER areasigER (bg ~1.8-2)\n')
		summary_file.close
		summary_file = open(currDir + '/summary.txt', 'a')
		summarySigNorm = []
		summaryERNorm = []
		summarySig = []
		summaryER = []
		summaryArea = []
		summaryAreaER = []
		
		for filename in ifiles:
			IJ.log("intensityAllCells.py: Compute image intensity file %s" %filename)
			signalImg, signalNorm, signal, area, signalImgER, signalERNorm, signalER, areaER = runOnFile(currDir, filename)
			IJ.saveAs(signalImg, "Tiff", filename + '_mask.tif')
			summarySigNorm.append(signalNorm)
			summaryERNorm.append(signalERNorm)
			summarySig.append(signal)
			summaryER.append(signalER)
			summaryArea.append(area)
			summaryAreaER.append(areaER)
			IJ.log("intensityAllCells.py: finished processing %s" %filename)
		#calculate time vector
		time = [j*DT for j in range(0, len(summarySig[0]))]
		for j in range(0, len(summarySig[0])):
			tmp = str(time[j])
			for i in range(0,len(summarySig)):
				tmp = tmp + " %.3f %.3f %.0f %.3f %.3f %.0f" %(summarySigNorm[i][j], summarySig[i][j], summaryArea[i][j], summaryERNorm[i][j], summaryER[i][j], summaryAreaER[i][j])
			summary_file.write(tmp + '\n')
		summary_file.close()

#dt is 15 min
DT = 15 
op = DirectoryChooser("Choose Directory...")
indir = op.getDirectory()
recursive = 1
if recursive:
	for root, dirs, files in os.walk(indir, topdown = False):
		executeOnDir(root, DT)
else:
	executeOnDir(indir, DT)


	