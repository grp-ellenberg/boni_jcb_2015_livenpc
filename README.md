# Boni_JCB_2015_LiveNPC

The images of human cells were provided by Andrea Boni and Jan Ellenberg.

Andrea Boni, Antonio Z. Politi, Petr Strnad, Wanqing Xiang, M. Julius Hossain and Jan Ellenberg (2015). Live imaging and modeling of inner nuclear membrane targeting reveals its molecular requirements in mammalian cells. J Cell Biol  209 (5): 705–720 / doi: 10.1083/jcb.201409133.

The images in this example are licensed as CC-0.
