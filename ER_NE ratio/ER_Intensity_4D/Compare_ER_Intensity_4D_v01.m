clear all; close all; clc

addpath 'cstruct'
addpath 'lsm'

%Type of processing
lowResolution = 0; % 1: yes (xyz have same resolution, 0: no - (generates additional z slices) 

%Structuring element_1
se=ones(3,3,3);
se(1,1,1) = 0;
se(1,1,3) = 0;
se(1,3,1) = 0;
se(1,3,3) = 0;
se(3,1,1) = 0;
se(3,1,3) = 0;
se(3,3,1) = 0;
se(3,3,3) = 0;





intDecay = 0.15; % Intensity correction factor along z - proportion of highest gain;
%Define the maximum number of subplots to disply intermediate results
disRow = 4;
disCol = 5;
rCRegion = 2;
minCellVol = 2250;

%Filtering parameters
if (lowResolution == 1)
    hsize=3; %Kernel size for gaussian blur
    sigma=2; %Sigma for gaussian blur
    smoothcontour = 15; % number of points used in smoothing
else
    hsize=5; %Kernel size for gaussian blur
    sigma=2.5; %Sigma for gaussian blur
    smoothcontour = 50; %number of points used in smoothing
end


fn = 'Stack_1.lsm'; %name of input file
fp = '..\Sample_Stack\'; % directory of input file
disp(sprintf('fn = %s', fn));

if lowResolution == 0
    outDir = [fp(1:end-11) 'Result\' fp(end-10:end) fn(1:end-4) '_HighRes' '\']; % Output directory to store the results
    outDirImg = [fp(1:end-11) 'Result\' fp(end-10:end) fn(1:end-4) '_HighRes\' 'Images\'];
else
    outDir = [fp(1:end-11) 'Result\' fp(end-10:end) fn(1:end-4) '_LowRes' '\']; % Output directory to store the results
    outDirImg = [fp(1:end-11) 'Result\' fp(end-10:end) fn(1:end-4) '_LowRes\' 'Images\'];
end

if ~exist(outDir)
    mkdir(outDir);
    mkdir(outDirImg);
end

lsminf = lsminfo([fp fn]); %lsm header
Nt=lsminf.TIMESTACKSIZE; % number of timepoint
NzOrig=lsminf.DIMENSIONS(3); % number of Z slices in lsm file
Nc=lsminf.NUMBER_OF_CHANNELS; % number of channels

tinit = 1; % Set higher than one if you do not want to process some timepoints before tinit
tfin = Nt; % Set lower than Nt if you do not wish to process some time points after tfin
zinit = 1; % Set higher than one if you do not want to process some lower slices

%Size of voxels in x, y and z: They changes based on how we process
%lowRes/highRes
voxelSizeX = lsminf.VoxelSizeX * 1e6;
voxelSizeY = lsminf.VoxelSizeY * 1e6;
voxelSizeZ = lsminf.VoxelSizeZ * 1e6;

if lowResolution == 0
    zFactor = round(voxelSizeZ/voxelSizeX);
    scalingfactor = 1;
    landStackOrig=zeros(lsminf.DIMENSIONS(1),lsminf.DIMENSIONS(2),NzOrig-zinit+1);
    landStack=zeros(lsminf.DIMENSIONS(1), lsminf.DIMENSIONS(2),(NzOrig-zinit+1)*zFactor - zFactor +1);
    if Nc>1
        protStackOrig=zeros(lsminf.DIMENSIONS(1),lsminf.DIMENSIONS(2),NzOrig-zinit+1);
        protStack=zeros(lsminf.DIMENSIONS(1), lsminf.DIMENSIONS(2),(NzOrig-zinit+1)*zFactor - zFactor +1);
    end
else
    zFactor = 1;
    scalingfactor = (voxelSizeX/voxelSizeZ);
    landStackOrig=zeros(lsminf.DIMENSIONS(1),lsminf.DIMENSIONS(2),NzOrig-zinit+1);
    landStack=zeros(ceil(lsminf.DIMENSIONS(1)*scalingfactor),ceil(lsminf.DIMENSIONS(2)*scalingfactor),NzOrig-zinit+1);
    if Nc >1
        protStackOrig=zeros(lsminf.DIMENSIONS(1),lsminf.DIMENSIONS(2),NzOrig-zinit+1);
        protStack=zeros(ceil(lsminf.DIMENSIONS(1)*scalingfactor),ceil(lsminf.DIMENSIONS(2)*scalingfactor),NzOrig-zinit+1);
    end
end

%Processing direction -1:backward, +1 forward - Keep it -1 always since it
%will change its value automatically after processing tinit.
direction = -1;
twoCentFlag = 0;

%Number of iteration to extract the cytoplasm intensity
nBlobs = 10; %Maximum number of blobs to be analysed

tStart = 1;

tpoint = tStart;
while tpoint <=tfin
    for zplane=zinit:NzOrig
        stacklsm = tiffread31([fp fn],2*((tpoint-1)*NzOrig+zplane-zinit+1)-1);
        if Nc >1
            landStackOrig(:,:,zplane-zinit+1) = stacklsm.data{1};
            protStackOrig(:,:,zplane-zinit+1) = stacklsm.data{2};
            if lowResolution == 1 %Generate low resolution stacks
                landStack(:,:,zplane-zinit+1) = imresize(landStackOrig(:,:,zplane-zinit+1),scalingfactor,'bicubic');
                protStack(:,:,zplane-zinit+1) = imresize(protStackOrig(:,:,zplane-zinit+1),scalingfactor,'bicubic');
            end
        else
            landStackOrig(:,:,zplane-zinit+1) = stacklsm.data;
            if lowResolution == 1 %Generate low resolution stacks
                landStack(:,:,zplane-zinit+1) = imresize(landStackOrig(:,:,zplane-zinit+1),scalingfactor,'bicubic');
            end
        end
        clear stacklsm
    end
    
    %Diplay input image stack
    %displaySubplots(landStackOrig, 'Diplay input image stack', disRow, disCol, NzOrig, 1, 2);
    
    disp(sprintf('tpoint = %g', tpoint));
    
    if lowResolution == 0 %Generate intermediate z slices 
        landStack = genIntermediateSlices(landStackOrig, zFactor);
        if Nc>1
            protStack = genIntermediateSlices(protStackOrig, zFactor);
        end
    end
    clear landStackOrig;
    clear protStackOrig;
    
    
    if tpoint == tStart
        if lowResolution == 1 %Equal pixel size in x,y and z
            voxelSizeX = voxelSizeZ;
            voxelSizeY = voxelSizeZ;
        else
            voxelSizeZ = voxelSizeZ/zFactor; % voxelsize in z gets closer to that in xy
        end
        [dx dy Nz] = size(landStack); % getting size of the volume to be processed further
        prevVol = zeros(dx,dy,Nz); % store current segmented volume  as a referece for next/prev time point
        prevIdx = zeros(dx,dy,Nz);
        tStartVol = zeros(dx,dy,Nz); % This used when processing in forward direction
        cRegion = zeros(dx,dy,Nz); %Store the detected cytoplasm region
        chRegion = zeros(dx,dy,Nz);%Store the detected chromosome region
        cRegionBord=zeros(dx,dy,Nz); %Store the boundary of cytoplasm
        %Intensity correction index for diffferent slice using exponential gain
        %this is the basis of the correction, but the exact parameters of the exponential function should 
        %be replaced with the optical properties of imaging
        corIndex = ExponentialGain(Nz,  2, 0.2, intDecay);
    end
    
    for i = 1: Nz
        landStack(:,:,i) = landStack(:,:,i)* corIndex(i);
        protStack(:,:,i) = protStack(:,:,i)* corIndex(i);
    end
    
    %Apply gussian filter in 3D
    landStackBack = landStack;
    protStackBack = protStack;
    landStack = imgaussian(landStack,sigma,hsize);
    protStack = imgaussian(protStack, sigma, hsize);
    voxelSize = voxelSizeX * voxelSizeY * voxelSizeZ;
    
    %Display filtered data
    %displaySubplots(landStack, 'Filtered image stack', disRow, disCol, Nz, zFactor, 2);
    
    %% Adaptive thresholding
    [chThresh3D, histProt] = Otsu_3D_Img(protStack, 0);
    chRegion(protStack(:,:,:) >= chThresh3D) = 1;
        
    for i=1:Nz
        chRegion(:,:,i) = imfill(chRegion(:,:,i), 'holes');
    end
    
    threeDLabel = bwconncomp(chRegion);
    numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
    
    chRegion(:,:,:) = 0;
    for i = 1:length(numPixels)
        if numPixels(i) * voxelSizeX * voxelSizeY >= 20
            chRegion(threeDLabel.PixelIdxList{i}) = 1;
        end
    end
 

    chRegionExt(:,:,:) = chRegion;

    chRegionSrk(:,:,:) = imerode(chRegion, se);
    chRegionSrk(:,:,:) = imerode(chRegionSrk, se);

    %displaySubplots(chRegionExt, 'Detected nucleus region', disRow, disCol, Nz, zFactor, 2);
    
    landStack(chRegionExt(:,:,:) == 1) = 0;         
    [cThresh3D, histLand] = Otsu_3D_Img(landStack, 0);
    cRegion(:,:,:) = 0;
    cRegion(landStack(:,:,:)>=cThresh3D) = 1;
    disp(sprintf('cThresh3D = %g', cThresh3D));
    
    cRegion(chRegionExt(:,:,:) ==1) = 1;
    totalBkgInt = sum(sum(sum(landStackBack(cRegion(:,:,:)==0))));
    totalBkgPixels = sum(sum(sum(cRegion(:,:,:) == 0)));
    avgBkgInt = totalBkgInt / totalBkgPixels;
       
    
    %Display detected ER region
    %displaySubplots(cRegion, 'Detected ER region', disRow, disCol, Nz, zFactor, 2);
    %Display Nucleus region
    %displaySubplots(chRegion, 'Detected Nucleus', disRow, disCol, Nz, zFactor, 2);
   
    
   

    %% 3 way filling of thresholded image
    
    for zplane = 1: Nz
        cRegion(:,:,zplane)=imdilate(cRegion(:,:,zplane),strel('disk',rCRegion,0));
        cRegion(:,:,zplane)=imfill(cRegion(:,:,zplane),'holes');
        for i=1:rCRegion
            cRegion(:,:,zplane)=imerode(cRegion(:,:,zplane),strel('diamond',1));
        end
    end

    %Display detected cell region
    %displaySubplots(cRegion, 'Detected ER region -filled', disRow, disCol, Nz, zFactor, 2);
    tempStack = landStackBack;
    tempStack(chRegionExt(:,:,:) ==0) = 0;
    tempStack(chRegionSrk(:,:,:) ==1) = 0;
    displaySubplots(tempStack, 'Necleus boundary', disRow, disCol, Nz, zFactor, 2);
    displaySubplots(landStackBack, 'Original Image', disRow, disCol, Nz, zFactor, 2);
    tempStack = landStackBack;
    nMRegion = xor(chRegionExt,chRegionSrk);
     tempStack(nMRegion(:,:,:) ==1) = 0;
     displaySubplots(tempStack, 'Original Image-ER on NM excluded', disRow, disCol, Nz, zFactor, 2);
    
    tempStack(nMRegion(:,:,:) == 0) = 0;
    displaySubplots(tempStack, 'Intensities on Nuclear membrane', disRow, disCol, Nz, zFactor, 2);
    
   
    %% End of filling

    %% Combining distance transform and watershed to identify cell region of interest
    distImage = bwdistsc(~cRegion,[1 1 voxelSizeZ/voxelSizeX]);
    
    %Display distance image
    %displaySubplots(distImage, 'Distance image', disRow, disCol, Nz, zFactor, 2);
        
    
    chRegion(cRegion(:,:,:) == 0) = 0;
    
    
    %Display initially detected chromosome
    %displaySubplots(chRegion, 'Chromosome - initial', disRow, disCol, Nz, zFactor, 2);
    
    marker = chRegion;
    %Putting markers on the boundary in xy planes where cell regions exist
    marker(imerode(cRegion(:,:,:), ones(3,3,3)) == 0) = 0;

    
%   disp(sprintf('threeDLabel.NumObjects = %g', threeDLabel.NumObjects));
%   disp(sprintf('markerThresh3D = %g', chThresh3D));
    
    %Display marker image
    %displaySubplots(marker, 'Marker image', disRow, disCol, Nz, zFactor, 2);

    distImage = -distImage; % invert distance image
    distImage = imimposemin(distImage,marker, 18); %suppress local minima other than markers
    %displaySubplots(distImage, 'DT after suppression', disRow, disCol, Nz, zFactor, 2);
    distImage(~cRegion) = -Inf; % Set outside of cell region with infinity
    %displaySubplots(distImage, 'DT after suppression - inf', disRow, disCol, Nz, zFactor, 2);
    wsLabel = watershed(distImage); %Apply watershed algorithm

    %Display watershed on distance image
    %displaySubplots(wsLabel, 'Watershed on distance image', disRow, disCol, Nz, zFactor, 2);
    
    wsLabel(~cRegion) = 0;
    %displaySubplots(wsLabel, 'Watershed on distance image - background removed', disRow, disCol, Nz, zFactor, 2);
    wsLabel(wsLabel>0) = 1;
    
    threeDLabel = bwconncomp(wsLabel,18); 
    numPixels = cellfun(@numel,threeDLabel.PixelIdxList); %Calculate  number of pixels in each connected component
    
    blobIdx = 1;
    chRegionBack = chRegion;
    chRegionExt = zeros(dx,dy,Nz);
    cellIdx = 1;
    while blobIdx <= min(length(numPixels), nBlobs)
        cRegion(:,:,:) = 0;
        [biggest,idx] = max(numPixels);
        
        if biggest *voxelSize <minCellVol
            break;
        end
        
        cRegion(threeDLabel.PixelIdxList{idx}) = 1;
        numPixels(idx) = 0;
        
        if tpoint > tStart
            if sum(sum(sum(prevVol.*cRegion))) < 0.33*sum(sum(sum(cRegion)))
                break;
            end
            
            idxImg = (prevIdx.*cRegion);
            numBlobs = min(length(numPixels), nBlobs);
            count = zeros(numBlobs,1);
            for i = 1:min(length(numPixels),nBlobs)
                count(i) = sum(sum(sum(idxImg(:,:,:) == i)));
            end
            [dummy, cellIdx] = max(count);
            clear count;
        end

        detVolume = sum(sum(sum(cRegion))) * voxelSize;
        disp(sprintf('detVolume = %g', detVolume));

        %Display selected blob using Watershed + DT
        %displaySubplots(cRegion, 'Selected blob using Watershed + DT', disRow, disCol, Nz, zFactor, 2);
         
        %% Smoothing cRegion
        flag = 0;
        cRegionBord(:,:,:) = 0;
        cRegionDis = cRegion;
        for zplane=1:Nz
            curBlob = findbiggestobject(cRegionDis(:,:,zplane),8);
            cRegionDis(:,:,zplane) = imsubtract(cRegionDis(:,:,zplane), curBlob);
            for row=1:size(curBlob,1)
                for col=1:size(curBlob,2)
                    if curBlob(row,col)
                        break;
                    end
                end
                if curBlob(row,col)
                    break;
                end
            end
            contourCRegion = bwtraceboundary(curBlob,[row col],'E');

            if(~isempty(contourCRegion))
                contourCRegion(:,1)=smooth(contourCRegion(:,1),smoothcontour);
                contourCRegion(:,2)=smooth(contourCRegion(:,2),smoothcontour);


                contourCRegion=[contourCRegion((floor(length(contourCRegion)/2)+1):length(contourCRegion),:);contourCRegion(1:floor(length(contourCRegion)/2),:)];
                contourCRegion(:,1)=smooth(contourCRegion(:,1),smoothcontour);
                contourCRegion(:,2)=smooth(contourCRegion(:,2),smoothcontour);

                if flag==0
                    contourCRegionThreeD=[contourCRegion(:,1),contourCRegion(:,2),zplane*ones(length(contourCRegion(:,1)),1)];
                else
                    contourCRegionThreeD=[contourCRegionThreeD;contourCRegion(:,1),contourCRegion(:,2),zplane*ones(length(contourCRegion(:,1)),1)];
                end
                flag = 1;

                for i=1:length(contourCRegion(:,1))
                    cRegionBord(round(contourCRegion(i,1)),round(contourCRegion(i,2)),zplane)=1;
                end
            end

            curBlob = findbiggestobject(cRegionDis(:,:,zplane),8);
            if sum(sum(curBlob)) < 100
                continue;
            end
            for row=1:size(curBlob,1)
                for col=1:size(curBlob,2)
                    if curBlob(row,col)
                        break;
                    end
                end
                if curBlob(row,col)
                    break;
                end
            end
            contourCRegion=bwtraceboundary(curBlob,[row col],'E');
            if(~isempty(contourCRegion))
                contourCRegion(:,1)=smooth(contourCRegion(:,1),smoothcontour);
                contourCRegion(:,2)=smooth(contourCRegion(:,2),smoothcontour);


                contourCRegion=[contourCRegion((floor(length(contourCRegion)/2)+1):length(contourCRegion),:);contourCRegion(1:floor(length(contourCRegion)/2),:)];
                contourCRegion(:,1)=smooth(contourCRegion(:,1),smoothcontour);
                contourCRegion(:,2)=smooth(contourCRegion(:,2),smoothcontour);

                if flag==0
                    contourCRegionThreeD=[contourCRegion(:,1),contourCRegion(:,2),zplane*ones(length(contourCRegion(:,1)),1)];
                else
                    contourCRegionThreeD=[contourCRegionThreeD;contourCRegion(:,1),contourCRegion(:,2),zplane*ones(length(contourCRegion(:,1)),1)];
                end
                flag = 1;

                for i=1:length(contourCRegion(:,1))
                    cRegionBord(round(contourCRegion(i,1)),round(contourCRegion(i,2)),zplane)=1;
                end
            end
            cRegion(:,:,zplane) = imfill(cRegionBord(:,:,zplane), 'holes');
        end
        clear cRegionDis;
        %% End of smoothing cRegion

        %% Detecting chromosome
        chRegion = chRegionBack;
        chRegion(cRegion(:,:,:)== 0) = 0;
                
        chRegionExt(:,:,:) = imdilate(chRegion(:,:,:), se);
        tempVol_2 = landStackBack;
        tempVol_2(cRegion(:,:,:) == 0) = 0;
        tempVol_2(chRegionExt(:,:,:) == 1) = 0;
        
        totalErInt = sum(sum(sum(tempVol_2)));
        tempVol_3 = tempVol_2;
        displaySubplots(tempVol_2, 'ER on cytoplasm', disRow, disCol, Nz, zFactor, 2);
        tempVol_2(:,:,:) = 0;
        tempVol_2(cRegion(:,:,:) == 1) = 1;
        tempVol_2(chRegionExt(:,:,:) == 1) = 0;
        totalErPixels = sum(sum(sum(tempVol_2(:,:,:))));
        totalErInt = totalErInt - avgBkgInt * totalErPixels;
        avgErInt = totalErInt/totalErPixels;
        
        chRegionExt(chRegionSrk(:,:,:) ==1) = 0;
        totalNmPixels = sum(sum(sum(chRegionExt)));
        tempVol = landStackBack;
        tempVol(chRegionExt(:,:,:) == 0) = 0; 
        displaySubplots(tempVol, 'ER on Nuclear membrane', disRow, disCol, Nz, zFactor, 2);
        totalNmInt = sum(sum(sum(tempVol))) - avgBkgInt * totalNmPixels;
        avgNmInt = totalNmInt/totalNmPixels;
             
        contourCRegionThreeD(:,1)=contourCRegionThreeD(:,1)* voxelSizeX;
        contourCRegionThreeD(:,2)=contourCRegionThreeD(:,2)* voxelSizeY;
        contourCRegionThreeD(:,3)=contourCRegionThreeD(:,3)* voxelSizeZ;

        %Set 1 in 8 corner points - to keep effective volume same in all
        %timepoint
        cRegionBord(1,1,1) = 1;
        cRegionBord(1,1,Nz) = 1;
        cRegionBord(1,dy,1) = 1;
        cRegionBord(1,dy,Nz) = 1;
        cRegionBord(dx,1,1) = 1;
        cRegionBord(dx,1,Nz) = 1;
        cRegionBord(dx,dy,1) = 1;
        cRegionBord(dx,dy,Nz) = 1;

        dumCol = zeros(dx,dy,Nz); % Just to have same color 

        [X,Y,Z]=meshgrid((1:dy)*voxelSizeY,(1:dx)*voxelSizeX,(1:Nz)*voxelSizeZ);

        hVol = figure('Name', strcat('Reconstructed 3D volume - timepoint: ', num2str(tpoint)));
        isosurface(X,Y,Z,cRegionBord,0.9);
        alpha(0.5)
        isosurface(X,Y,Z,chRegion,0.7);
        alpha(.7)
        isosurface(X,Y,Z,dumCol,0.5);
        alpha(1.0)
        hold on
        lbl = strcat('\mu','m');
        xlabel(lbl);
        ylabel(lbl)
        zlabel(lbl);

        daspect([1 1 1]);axis tight

        axis equal

        if tpoint<10
            savefile = [outDir fn(1:end-4) '_t0' num2str(tpoint) '_cell' num2str(cellIdx) '.fig'];
        else
            savefile = [outDir fn(1:end-4) '_t'  num2str(tpoint) '_cell' num2str(cellIdx) '.fig'];
        end
        saveas(hVol, savefile, 'fig');
        if tpoint<10
            savefile = [outDir fn(1:end-4) '_t0' num2str(tpoint) '_cell' num2str(cellIdx) '.tif'];
        else
            savefile = [outDir fn(1:end-4) '_t'  num2str(tpoint) '_cell' num2str(cellIdx) '.tif'];
        end
        saveas(hVol, savefile, 'tif');
        
        cRegionBord = imdilate(cRegionBord, ones(3,3,3));
        for i=1:Nz
            if tpoint<10 && i <10
                savefile = [outDirImg fn(1:end-4) '_PM_cell' num2str(cellIdx) '_t0' num2str(tpoint) '_z0' num2str(i)   '.tif'];
            elseif tpoint<10 && i >=10
                savefile = [outDirImg fn(1:end-4) '_PM_cell' num2str(cellIdx) '_t0' num2str(tpoint) '_z' num2str(i)   '.tif'];
            elseif tpoint>=10 && i <10
                savefile = [outDirImg fn(1:end-4) '_PM_cell' num2str(cellIdx) '_t' num2str(tpoint) '_z0' num2str(i)   '.tif'];
            else
                savefile = [outDirImg fn(1:end-4) '_PM_cell' num2str(cellIdx) '_t' num2str(tpoint) '_z' num2str(i)   '.tif'];
            end
            cRegionBord(:,:,i) = imfill(cRegionBord(:,:,i), 'holes');
            imwrite(cRegionBord(:,:,i), savefile, 'tif');
            
            if tpoint<10 && i <10
                savefile = [outDirImg fn(1:end-4) '_NM_cell' num2str(cellIdx) '_t0' num2str(tpoint) '_z0' num2str(i)   '.tif'];
            elseif tpoint<10 && i >=10
                savefile = [outDirImg fn(1:end-4) '_NM_cell' num2str(cellIdx) '_t0' num2str(tpoint) '_z' num2str(i)   '.tif'];
            elseif tpoint>=10 && i <10
                savefile = [outDirImg fn(1:end-4) '_NM_cell' num2str(cellIdx) '_t' num2str(tpoint) '_z0' num2str(i)   '.tif'];
            else
                savefile = [outDirImg fn(1:end-4) '_NM_cell' num2str(cellIdx) '_t' num2str(tpoint) '_z' num2str(i)   '.tif'];
            end
            imwrite(uint8(tempVol(:,:,i)), savefile, 'tif');
            
            if tpoint<10 && i <10
                savefile = [outDirImg fn(1:end-4) '_WC_cell' num2str(cellIdx) '_t0' num2str(tpoint) '_z0' num2str(i)   '.tif'];
            elseif tpoint<10 && i >=10
                savefile = [outDirImg fn(1:end-4) '_WC_cell' num2str(cellIdx) '_t0' num2str(tpoint) '_z' num2str(i)   '.tif'];
            elseif tpoint>=10 && i <10
                savefile = [outDirImg fn(1:end-4) '_WC_cell' num2str(cellIdx) '_t' num2str(tpoint) '_z0' num2str(i)   '.tif'];
            else
                savefile = [outDirImg fn(1:end-4) '_WC_cell' num2str(cellIdx) '_t' num2str(tpoint) '_z' num2str(i)   '.tif'];
            end
            imwrite(uint8(landStackBack(:,:,i)), savefile, 'tif');
            
            if tpoint<10 && i <10
                savefile = [outDirImg fn(1:end-4) '_ER_cell' num2str(cellIdx) '_t0' num2str(tpoint) '_z0' num2str(i)   '.tif'];
            elseif tpoint<10 && i >=10
                savefile = [outDirImg fn(1:end-4) '_ER_cell' num2str(cellIdx) '_t0' num2str(tpoint) '_z' num2str(i)   '.tif'];
            elseif tpoint>=10 && i <10
                savefile = [outDirImg fn(1:end-4) '_ER_cell' num2str(cellIdx) '_t' num2str(tpoint) '_z0' num2str(i)   '.tif'];
            else
                savefile = [outDirImg fn(1:end-4) '_ER_cell' num2str(cellIdx) '_t' num2str(tpoint) '_z' num2str(i)   '.tif'];
            end
            imwrite(uint8(tempVol_3(:,:,i)), savefile, 'tif');
            
        end
        
        if tpoint == 1
            xlswrite([outDir fn(1:end-4) '.xls'], {'Total_Volume'}, 1, 'A1');
            xlswrite([outDir fn(1:end-4) '.xls'], {'TotalNMInt'}, 1, 'B1');
            xlswrite([outDir fn(1:end-4) '.xls'], {'TotalCTInt'}, 1, 'C1');
            xlswrite([outDir fn(1:end-4) '.xls'], {'AverageNMInt'}, 1, 'D1');
            xlswrite([outDir fn(1:end-4) '.xls'], {'AverageCTInt'}, 1, 'E1');
            
            xlswrite([outDir fn(1:end-4) '.xls'], {'Total_Volume'}, 1, 'F1');
            xlswrite([outDir fn(1:end-4) '.xls'], {'TotalNMInt'}, 1, 'G1');
            xlswrite([outDir fn(1:end-4) '.xls'], {'TotalCTInt'}, 1, 'H1');
            xlswrite([outDir fn(1:end-4) '.xls'], {'AverageNMInt'}, 1, 'I1');
            xlswrite([outDir fn(1:end-4) '.xls'], {'AverageCTInt'}, 1, 'J1');
            
            xlswrite([outDir fn(1:end-4) '.xls'], {'Total_Volume'}, 1, 'K1');
            xlswrite([outDir fn(1:end-4) '.xls'], {'TotalNMInt'}, 1, 'L1');
            xlswrite([outDir fn(1:end-4) '.xls'], {'TotalCTInt'}, 1, 'M1');
            xlswrite([outDir fn(1:end-4) '.xls'], {'AverageNMInt'}, 1, 'N1');
            xlswrite([outDir fn(1:end-4) '.xls'], {'AverageCTInt'}, 1, 'O1');
            
            
            
        end
        letter = ('A' + 5*(cellIdx-1));
        xlswrite([outDir fn(1:end-4) '.xls'], detVolume,  1, strcat(letter, num2str(tpoint+1)));
        letter = ('A' + 5*(cellIdx-1)+1);
        xlswrite([outDir fn(1:end-4) '.xls'], totalNmInt, 1, strcat(letter, num2str(tpoint+1)));
        letter = ('A' + 5*(cellIdx-1)+2);
        xlswrite([outDir fn(1:end-4) '.xls'], totalErInt, 1, strcat(letter, num2str(tpoint+1)));
        letter = ('A' + 5*(cellIdx-1)+3);
        xlswrite([outDir fn(1:end-4) '.xls'], avgNmInt, 1, strcat(letter, num2str(tpoint+1)));
        letter = ('A' + 5*(cellIdx-1)+4);
        xlswrite([outDir fn(1:end-4) '.xls'], avgErInt, 1, strcat(letter, num2str(tpoint+1)));
        
        blobIdx = blobIdx + 1;
        if tpoint == tStart
            prevVol = or(prevVol, cRegion);
            prevIdx(:,:,:) = prevIdx(:,:,:)+ cRegion(:,:,:)*cellIdx;
        end            
        cellIdx = cellIdx + 1;
    end
    
    %% End of displaying volume and predicting parameters
    tpoint = tpoint + 1;
    
    clear wsLabel;
    clear distImage;
    clear marker;
    clear X;
    clear Y;
    clear Z;
    clear contourCRegionThreeD;
    clear dumCol;
    clear hist;   
end