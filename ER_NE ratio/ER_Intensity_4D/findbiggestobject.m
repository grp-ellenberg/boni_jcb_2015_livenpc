function biggestobject=findbiggestobject(image,connection)

[objects,objnum]=bwlabeln(image,connection);
objpos=cell(objnum,1);
volsize=zeros(objnum,1);
if objnum>1
    for i=1:objnum
        objpos{i}=find(objects==i);
        volsize(i)=size(objpos{i},1);
    end
    volmax=find(volsize==max(volsize));
    for j=1:length(volsize)
        if j~=volmax
            image(objpos{j})=0;
        end
    end    
end
biggestobject=image;