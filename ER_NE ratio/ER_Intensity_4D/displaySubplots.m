function displaySubplots(vol, figTitle, disRow, disCol, Nz, zFactor, inc)
%Display subplots from vol
%figTitle: title of the figure
figure('Name', figTitle);
for i = inc:inc:(Nz +zFactor - 1)/zFactor
    subplot(disRow,disCol,i/inc); imshow(vol(:,:,i*zFactor-zFactor +1),[]); hold on;
end
end