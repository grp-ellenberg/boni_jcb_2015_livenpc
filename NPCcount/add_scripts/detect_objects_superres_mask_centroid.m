function peak = detect_objects_superres_mask_centroid(orig,w,cutoff,pth,orig2)


%====================================================================== 
% correlation length of camera noise (usu. set to unity)
lambdan = 1;
% some often used quantities
idx = -w:1:w;     % index vector
dm = 2*w+1;         % diameter
im = repmat(idx',1,dm);
jm = repmat(idx,dm,1);
imjm2 = im.^2+jm.^2;
[ypixels,xpixels] = size(orig);   % image size
%figure (1), imshow(orig), title('Original Image');


%% crocker and grier preprocessing
%build kernel K for background extraction and noise removal
%(eq. [4])
B = sum(exp(-(idx.^2/(4*lambdan^2))));
B = B^2;
K0 = 1/B*sum(exp(-(idx.^2/(2*lambdan^2))))^2-(B/(dm^2));
K = (exp(-(imjm2/(4*lambdan^2)))/B-(1/(dm^2)))/K0;
%the raw GSD-images were filtered with (fspecial gaussian 11,4 *9000),
%to prevent saturated imaged when converting to 16bit, divide intesities
%by 3 first
orig=orig/3;
filtered = conv2(orig,K,'same');
filtered = uint16(filtered);

%structural elements for bwmorph operations e.g.
se = strel('disk',w);
se_2 = strel('disk',1);

%====================================================================== 
% STEP 2: Locating particles
%====================================================================== 
%% finding maxima 
% grayscale dilation - goes over all pixels and checks user-defined
% environment se, replaces grayvalues with the highest intensity value
% found. then it substracts the original from the grayscale-dilated, which
% leaves the local maxima as zero-values. it is best to choose se to be a
% smaller then the minimal distance between maxima, otherwise some maxima
% might be lost. for my movies se = strel('disk', 2) worked best.

%% finding maxima with grayscale-dilation
I_dilated = imdilate(filtered,se); % grayscale dilation
negative = I_dilated-filtered; % in negative maxima have intensity value 0
[x,y] = find(negative==0);
siz=[ypixels xpixels];
maxima = zeros(siz);
maxima(sub2ind(siz,x,y)) = 1;

image_index_max=xpixels*ypixels;
for image_index=1:image_index_max
    if filtered(image_index) == 0
        maxima(image_index)==0;
    end
end;

% figure;
% imshow(maxima);
% set(gca,'Ydir','reverse');

fgm_endpoints = bwmorph(maxima,'endpoints',1);

%removing maxima consisting of more than 1 pixel, connectivity 8
%sidenote, room for improvement: even extensive elongated maxima, which are
%a hint for 2 close peaks, are cut so only one remains...

struct_conncomp=bwconncomp(fgm_endpoints,8); %   
maxIDlist=struct_conncomp.PixelIdxList;
num_max=length(maxIDlist);
for max_count=1:num_max
    maxIDlist_mod{1,max_count}(1,1)=maxIDlist{1,max_count}(1,1);
end
fgm_mod=zeros(ypixels,xpixels);
for max_count=1:num_max
    fgm_mod(maxIDlist_mod{1,max_count}(1,1))=1;
end

%% introduce percentile of brightest, filter out all maxima which are less bright

% determining upper pth-th percentile of intensity values
pth = 0.01*pth;
[cnts,bins] = imhist(filtered);
l = length(cnts);
k = 1;
while sum(cnts(l-k:l))/sum(cnts) < pth,
    k = k + 1;
end;
thresh = bins(l-k+1)

% filter out maxima which are too dim
fgm_mod_new=fgm_mod;
image_index_max=xpixels*ypixels;
for image_index=1:image_index_max
    if filtered(image_index) <= thresh
        fgm_mod_new(image_index)=0;
    end
end;

%[coord_x,coord_y]=ind2sub(size(fgm_mod_new),find(fgm_mod_new));

%% make mask around cluster centers, disk with radius=10

se_disk10=strel('disk',10);
cluster_mask=imdilate(fgm_mod_new,se_disk10);
gradmag=zeros(ypixels,xpixels);

seg_rim=imdilate(cluster_mask,se_2)-cluster_mask;
all_pixels=xpixels*ypixels;
    
    %basin
    for pixel_i=1:all_pixels
        if cluster_mask(pixel_i)==1
        gradmag(pixel_i)=1;
        end
    end

    %deepest point in basins
    for pixel_i=1:all_pixels
        if fgm_mod_new(pixel_i)==1
        gradmag(pixel_i)=-Inf;
        end
    end
    
    %walls
    for pixel_i=1:all_pixels
        if seg_rim(pixel_i)==1
        gradmag(pixel_i)=+Inf;
        end
    end
    
result1=watershed(gradmag,4);

npart=max(max(result1)); 
lacking_max_counter=0;
for iParticle=1:npart
    [iRow,iCol]=find(result1==iParticle);
    label_indeces=sub2ind([ypixels xpixels],iRow,iCol);
    single_regions=zeros(ypixels,xpixels);
    single_regions(label_indeces)=1;
    single_regions_dil=imdilate(single_regions,se_2);
    single_rim=single_regions_dil-single_regions;
    [iRow_rim,iCol_rim]=find(single_rim==1);
    label_indeces_rim=sub2ind([ypixels xpixels],iRow_rim,iCol_rim);
    [length_s,width_s]=size(label_indeces_rim);
    single_regions_patched=single_regions;
    num_rim=length_s;
    max_rim_index=xpixels*ypixels; 
    
    [iRow_single,iCol_single]=find(single_regions_patched==1);
    label_indeces_single_patched=sub2ind([ypixels xpixels],iRow_single,iCol_single);
    
    % if max_or_not=0 no maximum in region
    max_or_not=sum(fgm_mod_new(label_indeces_single_patched));
    if max_or_not==0
        result1(label_indeces)=0;
        lacking_max_counter=lacking_max_counter+1;
    else 
        result1(label_indeces)=iParticle-lacking_max_counter;
    end
    
end

result1;
result2=label2rgb(result1,'jet','w','shuffle');

%% the brightest pixel is often not the cluster center, thus after detecting preliminary cluster centers,
%  I check the environment and reset the cluster center to the weighted
%  centroid within the prelimiary cluster, in principle one could do this
%  iteratively for several times and set a stopping-criterion, but for
%  andreas problem one round was good enough...

s=regionprops(result1,filtered,'WeightedCentroid');

measuredCentroid=zeros(numel(s),2);
for k=1:numel(s)
    measuredCentroid(k,1:2)=round(s(k).WeightedCentroid);
    CentroidInd(k,1) = sub2ind([ypixels xpixels], measuredCentroid(k,2), measuredCentroid(k,1));
end

fgm_mod_final=zeros(ypixels,xpixels);
num_max=length(CentroidInd);

for max_count=1:num_max
    fgm_mod_final(CentroidInd(max_count))=1;
end

clear CentroidInd measuredCentroid

% h=figure;
% imshow(orig2/100), hold on
% himage2=imshow(result2);
% set(himage2,'AlphaData', 0.1);
% set(gca,'Ydir','reverse');
% hold off




%% make mask around cluster centers, disk with radius=10

%se_disk10=strel('disk',10);
cluster_mask_final=imdilate(fgm_mod_final,se_disk10);
gradmag_final=zeros(ypixels,xpixels);

seg_rim_final=imdilate(cluster_mask_final,se_2)-cluster_mask_final;
all_pixels=xpixels*ypixels;
    
    %basin
    for pixel_i=1:all_pixels
        if cluster_mask_final(pixel_i)==1
        gradmag_final(pixel_i)=1;
        end
    end

    %deepest point in basins
    for pixel_i=1:all_pixels
        if fgm_mod_final(pixel_i)==1
        gradmag_final(pixel_i)=-Inf;
        end
    end
    
    %walls
    for pixel_i=1:all_pixels
        if seg_rim_final(pixel_i)==1
        gradmag_final(pixel_i)=+Inf;
        end
    end
    
result1_final=watershed(gradmag_final,4);

npart=max(max(result1_final)); 
lacking_max_counter=0;
for iParticle=1:npart
    [iRow,iCol]=find(result1_final==iParticle);
    label_indeces=sub2ind([ypixels xpixels],iRow,iCol);
    single_regions=zeros(ypixels,xpixels);
    single_regions(label_indeces)=1;
    single_regions_dil=imdilate(single_regions,se_2);
    single_rim=single_regions_dil-single_regions;
    [iRow_rim,iCol_rim]=find(single_rim==1);
    label_indeces_rim=sub2ind([ypixels xpixels],iRow_rim,iCol_rim);
    [length_s,width_s]=size(label_indeces_rim);
    single_regions_patched=single_regions;
    num_rim=length_s;
    max_rim_index=xpixels*ypixels; 
    
    [iRow_single,iCol_single]=find(single_regions_patched==1);
    label_indeces_single_patched=sub2ind([ypixels xpixels],iRow_single,iCol_single);
    
    % if max_or_not=0 no maximum in region
    max_or_not=sum(fgm_mod_final(label_indeces_single_patched));
    if max_or_not==0
        result1_final(label_indeces)=0;
        lacking_max_counter=lacking_max_counter+1;
    else 
        result1_final(label_indeces)=iParticle-lacking_max_counter;
    end
    
end

result2_final=label2rgb(result1_final,'jet','w','shuffle');

%h=figure;
%imshow(orig2/100), hold on
% himage3=imshow(result2_final);
% set(himage3,'AlphaData', 0.1);
% set(gca,'Ydir','reverse');
% hold off

%saveas(h,name,'png')


%%
% figure;
% imshow(fgm_mod)';
% set(gca,'Ydir','reverse');
% writing result into "peak"
 
 peak = result1_final;   
% peak(:,2) = coord_y;    
% peak(:,4) = measuredMajorAxisLength; 
% peak(:,5) = measuredArea;
% %field 6: used by linker to store linked list indices
% peak(:,7) = measuredMeanIntensity;
% peak(:,8) = measuredCircularity; %circularity
peak = {peak} 
clearvars -except peak negative 
return
