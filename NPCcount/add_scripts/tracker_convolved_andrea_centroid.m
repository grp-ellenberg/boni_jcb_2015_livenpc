

function peaks = tracker_convolved_andrea_centroid(filestub, ext, init, final,filestub2)

%=============================================================
% User-adjustable parameters below
%=============================================================

%----------------------------------------------------
% general parameters
%----------------------------------------------------
if length(filestub) == 0,
    filestub = '';   % default stub of image file names
end;
if length(ext) == 0,
    ext = '';   % default extension of image file names
end;
para = 0;      % parallel (1) or serial (0) version?
parsys = 'MPI';  % 'PM'  % parMATLAB (PM) or MPI (MPI) ?
viz = 0;       % visulaize images?
% list of machine names for parallel execution using MPI.
% The list can be of any length > 0.
machines = {};

%----------------------------------------------------
% parameters for detect_particles, link_trajectories
%----------------------------------------------------
w = 10;         % radius of neighborhood: > particle radius, 
               % < interparticle distance
cutoff = 0;  % probability cutoff for non-particle
               % discrimination
pth = 12;       % upper intensity percentile for particles
L = 3;        % maximum displacement between frames

%-------- No more user-adjustable settings below this line ---------
disp('Scanning files for minimum and maximum intensity values...')
maxint = -Inf;
minint = Inf;
numsat = 0;

%wani_mod

tic
for img=init:final,
  
    image_dig=img; 
    image_dig=sprintf('%03d',img);
    %file = sprintf('%s%s%s',filestub,num2str(image_dig),ext);
    file = filestub;
    file2 = filestub2;
    orig = double(imread(file));
    orig2 = double(imread(file2));
    locmax = max(max(orig));
    locmin = min(min(orig));
    if locmax > 254, numsat = numsat+1; end;
    if locmax > maxint, maxint = locmax; end;
    if locmin < minint, minint = locmin; end;
    images(:,:,img-init+1) = orig;
    images2(:,:,img-init+1) = orig2;
    clear image_dig
   
end
toc

nimg = final-init+1;
disp(sprintf('%d frame images successfully read',nimg))
disp(sprintf('Minimum intensity value is: %f',minint))
disp(sprintf('Maximum intensity value is: %f',maxint))
if numsat > 0,
    disp(sprintf('WARNING: found %d saturated pixels !',numsat))
end;


%=============================================================
% detect particles
%=============================================================

    peaks = [];
    for img=1:nimg,
        disp(sprintf('\nParticle recoginition in image %d of %d',img,nimg))
        if viz == 1,
            figure(nfig)
            nfig = nfig + 1;
            imshow(images(:,:,img))
            title('Original micrograph');
        end;
        peak = detect_objects_superres_mask_centroid(images(:,:,img),w,cutoff,pth,images2(:,:,img));
        peaks = [peaks, peak];
    end;









 