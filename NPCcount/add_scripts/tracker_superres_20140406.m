

function [peaks] = tracker_20130314(filestub, ext, init, final)

%----------------------------------------------------
% ignore para/viz (no parallelization)
%----------------------------------------------------
if length(filestub) == 0,
    filestub = '';   % default stub of image file names
end;
if length(ext) == 0,
    ext = '';   % default extension of image file names
end;

para = 0;      
parsys = 'MPI';  
viz = 0;       
machines = {};

maxint = -Inf;
minint = Inf;
numsat = 0;
%=============================================================
% User-adjustable parameters below
%=============================================================

w = 2;         % radius of neighborhood: > particle radius, 
               % < interparticle distance
cutoff = 3;  % probability cutoff for non-particle
               % discrimination
pth = 5;       % upper intensity percentile for particles


%-------- No more user-adjustable settings below this line ---------

for img=init:final,
  
    image_dig=img; 
    image_dig=sprintf('%03d',img);
    file = filestub;
    orig = double(imread(file));
    locmax = max(max(orig));
    locmin = min(min(orig));
    if locmax > 254, numsat = numsat+1; end;
    if locmax > maxint, maxint = locmax; end;
    if locmin < minint, minint = locmin; end;
    images(:,:,img-init+1) = orig;
    clear image_dig
   
end


nimg = final-init+1;
disp(sprintf('%d frame images successfully read',nimg))
disp(sprintf('Minimum intensity value is: %f',minint))
disp(sprintf('Maximum intensity value is: %f',maxint))
if numsat > 0,
    disp(sprintf('WARNING: found %d saturated pixels !',numsat))
end;

    peaks = [];
    for img=1:nimg,
        disp(sprintf('\nParticle recognition in image %d of %d',img,nimg))
        if viz == 1,
            figure(nfig)
            nfig = nfig + 1;
            imshow(images(:,:,img))
            title('Original micrograph');
        end;
        [peak] = detect_objects_superres(images(:,:,img),w,cutoff,pth);
        peaks = [peaks, peak];

    end;



 