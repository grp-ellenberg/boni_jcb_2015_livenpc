function peak = detect_objects_superres(orig,w,cutoff,pth)

%====================================================================== 
% STEP 1: Image restoration is done in ICY-protocol already
%====================================================================== 
% correlation length of camera noise (usu. set to unity)
lambdan = 1;
% some often used quantities
idx = -w:1:w;     % index vector
dm = 2*w+1;         % diameter
im = repmat(idx',1,dm);
jm = repmat(idx,dm,1);
imjm2 = im.^2+jm.^2;
[ypixels,xpixels] = size(orig);   % image size
%figure (1), imshow(orig), title('Original Image');


%% crocker and grier preprocessing
%build kernel K for background extraction and noise removal
%(eq. [4])
B = sum(exp(-(idx.^2/(4*lambdan^2))));
B = B^2;
K0 = 1/B*sum(exp(-(idx.^2/(2*lambdan^2))))^2-(B/(dm^2));
K = (exp(-(imjm2/(4*lambdan^2)))/B-(1/(dm^2)))/K0;
%the raw GSD-images were filtered with (fspecial gaussian 11,4 *9000),
%to prevent saturated imaged when converting to 16bit, divide intesities
%by 3 first
orig=orig*3000;
filtered = conv2(orig,K,'same');
filtered = uint16(filtered);

%structural elements for bwmorph operations e.g.
se = strel('disk',w);
se_2 = strel('disk',1);

%====================================================================== 
% STEP 2: Locating particles
%====================================================================== 
%% finding maxima 
% grayscale dilation - goes over all pixels and checks user-defined
% environment se, replaces grayvalues with the highest intensity value
% found. then it substracts the original from the grayscale-dilated, which
% leaves the local maxima as zero-values. it is best to choose se to be a
% smaller then the minimal distance between maxima, otherwise some maxima
% might be lost. for my movies se = strel('disk', 2) worked best.

%% finding maxima with grayscale-dilation
I_dilated = imdilate(filtered,se); % grayscale dilation
negative = I_dilated-filtered; % in negative maxima have intensity value 0
[x,y] = find(negative==0);
siz=[ypixels xpixels];
maxima = zeros(siz);
maxima(sub2ind(siz,x,y)) = 1;

image_index_max=xpixels*ypixels;
for image_index=1:image_index_max
    if filtered(image_index) == 0
        maxima(image_index)==0;
    end
end;

% figure;
% imshow(maxima);
% set(gca,'Ydir','reverse');

fgm_endpoints = bwmorph(maxima,'endpoints',1);

%removing maxima consisting of more than 1 pixel, connectivity 8
%sidenote, room for improvement: even extensive elongated maxima, which are
%a hint for 2 close peaks, are cut so only one remains...

struct_conncomp=bwconncomp(fgm_endpoints,8); %   
maxIDlist=struct_conncomp.PixelIdxList;
num_max=length(maxIDlist);
for max_count=1:num_max
    maxIDlist_mod{1,max_count}(1,1)=maxIDlist{1,max_count}(1,1);
end
fgm_mod=zeros(ypixels,xpixels);
for max_count=1:num_max
    fgm_mod(maxIDlist_mod{1,max_count}(1,1))=1;
end

%% introduce percentile of brightest, filter out all maxima which are less bright
%  this is a routine from the sbalzarini-original that i took out for my
%  movies since i clean up the background with the level-set thresholding
%  and then don't have problems with peaks detected in background

% determining upper pth-th percentile of intensity values
pth = 0.01*pth;
[cnts,bins] = imhist(filtered);
l = length(cnts);
k = 1;
while sum(cnts(l-k:l))/sum(cnts) < pth,
    k = k + 1;
end;
thresh = bins(l-k+1)

% filter out maxima which are too dim
fgm_mod_new=fgm_mod;
image_index_max=xpixels*ypixels;
for image_index=1:image_index_max
    if filtered(image_index) <= thresh
        fgm_mod_new(image_index)=0;
    end
end;

[coord_x,coord_y]=ind2sub(size(fgm_mod_new),find(fgm_mod_new));


%%
% figure;
% imshow(fgm_mod)';
% set(gca,'Ydir','reverse');

%% writing result into "peak"
peak = zeros(length(coord_x),2);
peak(:,1) = coord_x;   
peak(:,2) = coord_y;    
peak = {peak} 
clearvars -except peak negative 
return

