clear all, close all

folderpath = uigetdir % folder containing further folders

dirlist = dir(strcat(folderpath,filesep,'*.tif'));
traj_all=cell([2 length(dirlist)]);

%% detecting the peaks with morphological opening
for movie_number=1:length(dirlist)
    
        filename=dirlist(movie_number).name(1:end);% name of folders
        selfnamed='overview';
        combinedfilename=strcat(selfnamed,filename)
        peaks=tracker_superres_20140406(sprintf('%s',folderpath,filesep,filename),'.tif',1,1);
        
        traj_all{1,movie_number}=filename;
        traj_all{2,movie_number}=peaks;
        
        clear peaks rearranged_peaks filtered_peaks matrices
end

[traj_l,traj_w]=size(traj_all)

