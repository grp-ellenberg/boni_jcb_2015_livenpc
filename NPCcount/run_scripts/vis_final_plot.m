%% assigning the cluster number to each detected subunit peak
for movie_number=1:length(dirlist2)
        for ii=1:length(traj_all{2,movie_number}{1,1})
        traj_all{2,movie_number}{1,1}(ii,3)=cluster_mask{2,movie_number}{1,1}(traj_all{2,movie_number}{1,1}(ii,1),traj_all{2,movie_number}{1,1}(ii,2));
        end
end

%% counting number of peaks within each cluster
for movie_number=1:length(dirlist2)
    for ii=1:max(traj_all{2,movie_number}{1,1}(:,3))
        traj_all{3,movie_number}{1,1}(ii,1)=length(find(traj_all{2,movie_number}{1,1}(:,3)==ii));
    end
end

%% pooling all of the counted peaks within clusters of one dataset
cluster_counts=traj_all{3,1}{1,1}(:,1);

for movie_number= 2:length(dirlist2) % loops starts with 2 since 1st entry is in already
    cluster_counts=vertcat(cluster_counts,traj_all{3,movie_number}{1,1}(:,1));
end






 