folderpath = uigetdir('pick filtered for CLUSTER detection'); % folder containing further folders
folderpath2 = uigetdir('pick filtered for PEAK detection');

dirlist = dir(strcat(folderpath,filesep,'*.tif'));
dirlist2 = dir(strcat(folderpath2,filesep,'*.tif'));
cluster_mask=cell([2 length(dirlist)]);

%% detecting the peaks with morphological opening
for movie_number=1:length(dirlist)
        filename=dirlist(movie_number).name(1:end);% name of folders
        filename2=dirlist2(movie_number).name(1:end);% name of folders
        peaks=tracker_convolved_andrea_centroid(sprintf('%s',folderpath,filesep,filename),'.tif',1,1,sprintf('%s',folderpath2,filesep,filename2));
        cluster_mask{1,movie_number}=filename;
        cluster_mask{2,movie_number}=peaks;
 end



